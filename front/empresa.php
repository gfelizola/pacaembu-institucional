<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="sixteen columns">
            <div class="row">
                <div class="eight columns">
                    <h3 class="titulo preto">A empresa</h3>
                </div>

                <div class="sixteen columns">
                    <ul class="breadcrumbs">
                        <li><a href="#nossa-equipe">nossa equipe</a></li>
                        <li><a href="#certificados">certificados</a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="row">
                    <div class="twentyfour columns">
                        <p>Desde o início de suas atividades, a Pacaembu Empreendimentos vêm participando ativamente das transformações do mercado construtor, atuando principalmente no estado de São Paulo, executando obras habitacionais destinadas a população de baixa renda.</p>
                        <p>Dentre eles, conjuntos habitacionais horizontais e verticais, loteamentos, condomínios residenciais, além de outras obras públicas tais como: creches, postos de saúde, centros comunitários, escolas, etc.</p>
                        <p>Temos como política e objetivo a qualidade do nosso trabalho, o respeito, a valorização e o aperfeiçoamento de nossos colaboradores procurando padronizar os processos para formar uma equipe integrada e altamente profissional, mantendo assim a empresa sempre atualizada e competitiva, garantindo com isso o alto nível de qualidade.</p>
                        <p>A Pacaembu entra no mercado aliando a agilidade e a experiência de mais de 20 anos de seus sócios fundadores em construções, incorporações e loteamentos de baixa renda.</p>
                        <p>Ela nasce com um portfólio de inúmeras realizações em diversas cidades do Estado de São Paulo. Sua plataforma completa de negócios, com processos definidos, domínio de tecnologia construtiva e uma rede de fornecedores coesa, possibilitou um histórico de sucesso com clientes como a CDHU e a CAIXA ECONÔMICA FEDERAL, hoje o principal agente financiador de habitações no Brasil.</p>
                        <p>A construtora tem como principal foco de atuação as famílias com rendas entre 0 e 6 salários mínimos, participando de programas que contam com subsídio do Governo Federal, como o Programa Minha Casa Minha Vida.</p>
                        <p>Possuímos uma alta qualificação técnica e financeira, devidamente comprovada pela aprovação nos programas de qualidade PBQP-H. Nível A do Ministério das Cidades e ISO 9001, bem como aprovação na avaliação de crédito da Caixa Econômica Federal para empresas construtoras realizada pela sua gerência de risco (GERIC).</p>
                    </div>
                </div>

                <div class="row subarea" id="missao">
                    <div class="twelve columns">
                        <h3 class="titulo vermelho">Missão</h3>
                        <p class="preto linha-sobre"><strong>Desenvolver empreendimentos populares com alto padrão de qualidade a baixo custo possibilitando às famílias brasileiras o acesso a moradias dignas, de qualidade e a realização do sonho da casa própria.</strong></p>
                    </div>

                    <div class="twelve columns">
                        <h3 class="titulo vermelho">Visão</h3>
                        <p class="preto linha-sobre"><strong>Ser referência no desenvolvimento de empreendimentos de interesse social em termos de prazo, qualidade e baixo custo sendo assim reconhecida como a principal empresa deste segmento no Brasil.</strong></p>
                    </div>
                </div>

                <div class="row subarea" id="nossa-equipe">
                    <div class="twentyfour columns">
                        <h3 class="titulo preto">Nossa equipe</h3>
                        <p><strong>Nosso time de profissionais tem se destacado por diferenciais importantes:</strong></p>
                        <ul class="lista-setinha">
                            <li>Know-how na construção e incorporação de empreendimentos populares e bairros planejados;</li>
                            <li>Experiência comprovada em projetos habitacionais de interesse social para clientes como a CAIXA ECONÔMICA FEDERAL e CDHU;</li>
                            <li>Domínio de tecnologia construtiva de alvenaria estrutural e conjugação com sistemas construtivos industrializados;</li>
                            <li>Planejamento e programação de obras com grande volume de unidades;</li>
                            <li>Pioneira em trabalhar com crédito associativo no Brasil junto à CAIXA ECONÔMICA FEDERAL;</li>
                            <li>Conhecimento profundo dos processos para projetos junto a CAIXA ECONÔMICA FEDERAL.</li>
                        </ul>
                    </div>
                </div>

                <div class="row subarea" id="certificados">
                    <div class="twentyfour columns">
                        <h3 class="titulo preto">Certificados obtidos</h3>

                        <a href="images/certificado1.jpg" class="link-lupa fancybox" rel="certificados-group" >
                            <img src="images/certificado1_thumb.jpg" />
                            <span class="lupa">ver mais</span>
                        </a>

                        <a href="images/certificado2.jpg" class="link-lupa fancybox" rel="certificados-group">
                            <img src="images/certificado2_thumb.jpg" />
                            <span class="lupa">ver mais</span>
                        </a>

                        <a href="images/certificado3.jpg" class="link-lupa fancybox" rel="certificados-group">
                            <img src="images/certificado3_thumb.jpg" />
                            <span class="lupa">ver mais</span>
                        </a>

                        <a href="images/certificado4.jpg" class="link-lupa fancybox" rel="certificados-group">
                            <img src="images/certificado4_thumb.jpg" />
                            <span class="lupa">ver mais</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="eight columns">
            <div class="row">
                <div class="twentyfour columns">
                    <div class="noticias-twitter">
                        <h3>Notícias</h3>
                        <ul>
                            <li>
                                <a href="#@pacaembuconstru">@pacaembuconstru</a> O residencial tem localização privilegiada, vizinho aos condomínios Parque da Liberdade e Village Dahma Rio Preto.<br>
                                <span>3 dias atras.</span>
                            </li>
                            <li>
                                <a href="#@pacaembuconstru">@pacaembuconstru</a> O residencial tem localização privilegiada, vizinho aos condomínios Parque da Liberdade e Village Dahma Rio Preto.<br>
                                <span>3 dias atras.</span>
                            </li>
                            <li>
                                <a href="#@pacaembuconstru">@pacaembuconstru</a> O residencial tem localização privilegiada, vizinho aos condomínios Parque da Liberdade e Village Dahma Rio Preto.<br>
                                <span>3 dias atras.</span>
                            </li>
                            <li>
                                <a href="#@pacaembuconstru">@pacaembuconstru</a> O residencial tem localização privilegiada, vizinho aos condomínios Parque da Liberdade e Village Dahma Rio Preto.<br>
                                <span>3 dias atras.</span>
                            </li>
                        </ul>
                    </div>

                    <p><a href="empreendimentos.html"><img src="images/chamada-nossos-empreendimentos.jpg" /></a></p>
                    <p><a href="venda-seu-terreno.html"><img src="images/chamada-venda-seu-terreno.jpg" ></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>