<?php include 'header.php'; ?>
<?php 
    if( isset($_GET['id']) ){
        $id = $_GET['id'];
        $emp = $lancamentos[$id - 1];
    } else {
        header( 'Location: empreendimentos.php' ) ;
    }
?>
<div class="conteudo">
    <div class="row destaques">
        <div class="twentyfour columns">
            <div class="container-destaques">
                <img src="images/lancamento-destaque<?php echo "$id"; ?>.jpg" alt="">
                <div class="descritivo">
                    <h3 class="titulo branco">Descritivo</h3>
                    <p><?php echo $emp['descritivo']; ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="row subarea">
        <div class="eight columns">
            <h3 class="titulo vermelho linha-sob">Ficha Técnica</h3>
            <p><?php echo $emp['textos_blocos']; ?></p>
            <?php 
            foreach ($emp['blocos'] as $blk => $bloco) {
                ?>
                <div class="panel">
                    <p class="nome-bloco"><?php echo "$blk"; ?></p>
                    <p class="preto"><?php echo $bloco ?></p>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="eight columns">
            <h3 class="titulo vermelho linha-sob">Localização</h3>
            <p class="vermelho"><strong><?php echo $emp['cidade'] ?></strong></p>
            <div class="mapa">
                <div class="container-mapa">
                    <div id="map-canvas"></div>
                </div>
            </div>
            <p>Endereco: <strong><?php echo $emp['endereco'] ?></strong><br></p>
        </div>
        <div class="eight columns banners">
            <?php 
            foreach ($emp['banners'] as $banner) {
                ?>
                <p><img src="images/<?php echo $banner ?>" /></p>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>