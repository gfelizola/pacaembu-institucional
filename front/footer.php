<div class="footer-unidades">
        <div class="row">
            <div class="eight columns ipad">
                <img src="images/icone-ipad.png" />
                <p>Este site é acessível<br>
                    via <strong>iPad</strong> e outros<br>
                    dispositivos mobile</p>
            </div>
            <div class="sixteen columns unidades">
                <div class="row cima">
                    <div class="twelve columns unidade">
                        <h3>São Paulo <small>SP</small></h3>
                        <p>Rua Sabará, 566  - 7º andar<br>
                            Higienópolis - CEP 01239-010<br>
                            11 3236 4141</p>
                    </div>
                    <div class="twelve columns unidade">
                        <h3>Bauru <small>SP</small></h3>
                        <p>Av. Duque de Caxias, 11-70 2º andar<br>
                            Vila Altinópolis - CEP 17012-151<br>
                            14 3243 3060</p>
                    </div>
                </div>
                <div class="row">
                    <div class="twentyfour columns unidade">
                        <h3>São José do Rio Preto <small>SP</small></h3>
                        <p>Rua Saldanha Marinho, 2815 4º andar<br>
                            Centro - CEP 15010-100<br>
                            17 3304 6961</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="row menu-sitemap">
            <ul class="the_listmap">
                <li>
                    <a href="empresa.php">Empresa</a>
                    <ul>
                        <li><a href="empresa.php">Missão e visão</a></li>
                        <li><a href="empresa.php">Nossa equipe</a></li>
                        <li><a href="empresa.php">Certificados</a></li>
                    </ul>
                </li>
                <li>
                    <a href="empreendimentos.php">Empreendimentos</a>
                    <ul>
                        <li><a href="obras.php">Obras em andamento</a></li>
                    </ul>
                </li>
                <li>
                    <a href="empreendimentos.php">Lançamentos</a>
                    <ul>
                        <li><a href="obras.php">São Rogério</a></li>
                        <li><a href="obras.php">Casa Branca</a></li>
                        <li><a href="obras.php">Novo Horizonte</a></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li><a href="trabalhe-conosco.php">Trabalhe Conosco</a></li>
                        <li><a href="https://login.microsoftonline.com">Webmail</a></li>
                        <li><a href="
https://pacaembuconstrutoramicrosoftonlinecom-9.sharepoint.microsoftonline.com/default.aspx">Intranet</a></li>
                        <li><a href="fale-conosco.php">Fale conosco</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row direitos cf">
            <p>2013© Grupo Pacaembu Construtora - Todos os direitos reservados</p>
            <ul class="social">
                <li><a href="#fb" class="fb"></a></li>
                <li><a href="#tw" class="tw"></a></li>
                <li><a href="#in" class="in"></a></li>
            </ul>
        </div>
    </div>
</div>

<script src="javascripts/jquery.js"></script>
<script src="javascripts/foundation.min.js"></script>
<script src="javascripts/jquery.fancybox.pack.js"></script>
<script src="javascripts/jquery.fancybox-thumbs.js"></script>
<script src="javascripts/jquery.color.js"></script>
<script src="javascripts/flowplayer-3.2.6.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx8p9HH99lCwJCIcI1GwHVRZt-el0Ze1Y&sensor=false"></script>
<script src="javascripts/app.js"></script>
</body>
</html>
