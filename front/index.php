<?php include 'header.php'; ?>
<div class="row destaques">
    <div class="twentyfour columns">
        <div class="container-destaques">
            <div data-cation="#legenda1"><a href="empreendimentos-detalhe.php?id=1"><img src="images/empreendimento-destaque1.jpg" alt=""></a></div>
            <div data-cation="#legenda2"><a href="empreendimentos-detalhe.php?id=2"><img src="images/empreendimento-destaque2.jpg" alt=""></a></div>
            <div data-cation="#legenda3"><a href="empreendimentos-detalhe.php?id=3"><img src="images/empreendimento-destaque3.jpg" alt=""></a></div>
            <div data-cation="#legenda4"><a href="empreendimentos-detalhe.php?id=4"><img src="images/empreendimento-destaque4.jpg" alt=""></a></div>
            <div data-cation="#legenda5"><a href="empreendimentos-detalhe.php?id=5"><img src="images/empreendimento-destaque5.jpg" alt=""></a></div>
            <div data-cation="#legenda6"><a href="empreendimentos-detalhe.php?id=6"><img src="images/empreendimento-destaque6.jpg" alt=""></a></div>
            <div data-cation="#legenda7"><a href="empreendimentos-detalhe.php?id=7"><img src="images/empreendimento-destaque7.jpg" alt=""></a></div>
            <div data-cation="#legenda8"><a href="empreendimentos-detalhe.php?id=8"><img src="images/empreendimento-destaque8.jpg" alt=""></a></div>
            <div data-cation="#legenda9"><a href="empreendimentos-detalhe.php?id=9"><img src="images/empreendimento-destaque9.jpg" alt=""></a></div>
        </div>

        <span class="orbit-caption" id="legenda1"><a href="empreendimentos-detalhe.php?id=1">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda2"><a href="empreendimentos-detalhe.php?id=2">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda3"><a href="empreendimentos-detalhe.php?id=3">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda4"><a href="empreendimentos-detalhe.php?id=4">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda5"><a href="empreendimentos-detalhe.php?id=5">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda6"><a href="empreendimentos-detalhe.php?id=6">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda7"><a href="empreendimentos-detalhe.php?id=7">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda8"><a href="empreendimentos-detalhe.php?id=8">O sonho da casa própria começa aqui!</a></span>
        <span class="orbit-caption" id="legenda9"><a href="empreendimentos-detalhe.php?id=9">O sonho da casa própria começa aqui!</a></span>
    </div>
</div>

<div class="row">
    <div class="eight columns noticias-twitter">
        <h3>Notícias</h3>
        <ul>
            <li>
                <a href="#@pacaembuconstru">@pacaembuconstru</a> O residencial tem localização privilegiada, vizinho aos condomínios Parque da Liberdade e Village Dahma Rio Preto.<br>
                <span>3 dias atras.</span>
            </li>
            <li>
                <a href="#@pacaembuconstru">@pacaembuconstru</a> As casas do Residencial Luz da Esperança serão entregues com laje e piso cerâmico.<br>
                <span>3 dias atras.</span>
            </li>
            <li>
                <a href="#@pacaembuconstru">@pacaembuconstru</a> As famílias que vão ocupar os imóveis foram selecionadas pela Emcop, em 2011 durante processo de inscrição.<br>
                <span>3 dias atras.</span>
            </li>
            <li>
                <a href="#@pacaembuconstru">@pacaembuconstru</a> A entrega das casas será efetuada de acordo com a finalização dos módulos.<br>
                <span>3 dias atras.</span>
            </li>
            <li>
                <a href="#@pacaembuconstru">@pacaembuconstru</a> A entrega das casas será efetuada de acordo com a finalização dos módulos.<br>
                <span>3 dias atras.</span>
            </li>
        </ul>
    </div>
    <div class="eight columns chamada-destaque">
        <h3>Empreendimentos</h3>
        <a href="empreendimentos.php" class="link-lupa linha-sobre">
            <img src="images/home-empreendimentos.jpg" />
            <span class="lupa">ver mais</span>
        </a>
        <p>Saiba aonde estão e quais são os melhores empreendimentos realizados pela Pacaembu.</p>
        <!-- <iframe src="http://ipset.com.br/pacaembu/lealdadeamizade1_320.html" frameborder="0" width="280" height="240" scrolling="no"></iframe> -->
    </div>
    <div class="eight columns chamada-destaque">
        <h3>Obra Online</h3>
        <a class="camera-player" href="rtmp://68.232.187.245:1935/p61-lealami1/p61-lealami1.stream"></a>
        <p>Acompanhe nossos empreendimentos em tempo real através de cameras em nossas obras.</p>
    </div>
</div>
<div class="row">
    <div class="eight columns">
        <p><a href="empreendimentos.php"><img src="images/chamada-nossos-empreendimentos.jpg" /></a></p>
        <p><a href="venda-seu-terreno.php"><img src="images/chamada-venda-seu-terreno.jpg" ></a></p>
    </div>

    <div class="sixteen columns obras">
        <h3 class="linha-sobre linha-sob">obras em andamento</h3>
        <ul class="block-grid two-up">
            <li>
                <div class="porcentagem">
                    <div class="valor p98">98%</div>
                </div>
                <span>Residencial Jardim Bela Vista</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p73">73%</div>
                </div>
                <span>Res. São Rogério</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p89">89%</div>
                </div>
                <span>Luz da Esperança</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p7">7%</div>
                </div>
                <span>Res. Sto. Expedito</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p99">99%</div>
                </div>
                <span>Parque das Flores</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p64">64%</div>
                </div>
                <span>São Sebastião</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p18">18%</div>
                </div>
                <span>Harmonia</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p22">22%</div>
                </div>
                <span>Lealdade e Amizade</span>
            </li>
            <li>
                <div class="porcentagem">
                    <div class="valor p10">10%</div>
                </div>
                <span>Jardim Itália</span>
            </li>
        </ul>
    </div>
</div>
<?php include 'footer.php'; ?>