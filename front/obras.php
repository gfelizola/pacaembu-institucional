<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Obras em andamento</h3>
        </div>
        <div class="twelve columns">
            <ul class="breadcrumbs">
                <li><a href="#">Empreendimentos</a></li>
            </ul>
        </div>
    </div>

    <div class="row listagem-items">
        <ul class="block-grid two-up">
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/obras-fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Residencial Santo Expedito</a></h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake2.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake1.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake3.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake4.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake5.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake6.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake7.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake8.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake9.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <div class="container-img">
                        <img src="images/obras-fake10.jpg">
                    </div>
                </div>
                <div class="body">
                    <h4>Residencial Santo Expedito</h4>
                    <p><strong>São Paulo</strong><br>
                        Lapa<br>
                        2 ou 3 Dormitórios<br>
                        55 m² ou 69 m²</p>
                </div>
            </li>
        </ul>
    </div>
</div>
<?php include 'footer.php'; ?>