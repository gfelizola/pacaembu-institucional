<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Lançamentos</h3>
        </div>
    </div>

    <div class="row listagem-items">
        <ul class="block-grid two-up">
            <?php 
            $i = 1;
            foreach ($lancamentos as $emp) {
            ?>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="lancamentos-detalhe.php?id=<?php echo "$i"; ?>" class="container-img">
                        <img src="images/lancamento<?php echo "$i"; ?>.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="lancamentos-detalhe.php?id=<?php echo "$i"; ?>"><?php echo $emp["nome"]; ?></a></h4>
                    <p><strong><?php echo $emp["cidade"]; ?></strong></p>
                </div>
            </li>
            <?php 
            $i++;
            }
            ?>
        </ul>
    </div>
</div>
<?php include 'footer.php'; ?>