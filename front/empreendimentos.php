<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Empreendimentos</h3>
        </div>
        <div class="twelve columns">
            <ul class="breadcrumbs">
                <li><a href="obras.php">Obras em andamento</a></li>
                <li><a href="#mapa">Mapa</a></li>
            </ul>
        </div>
    </div>

    <div class="row listagem-items">
        <ul class="block-grid two-up">
            <?php 
            // $i = 1;
            foreach ($empreendimentos as $ie => $emp) :
                $i = intval($ie) + 1;
            ?>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="empreendimentos-detalhe.php?id=<?php echo "$i"; ?>" class="container-img">
                        <img src="images/empreendimento<?php echo "$i"; ?>.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="empreendimentos-detalhe.php?id=<?php echo "$i"; ?>"><?php echo $emp["nome"]; ?></a></h4>
                    <p><strong><?php echo $emp["cidade"]; ?></strong><br>
                        <?php echo $emp["quartos"]; ?><br>
                        <?php echo $emp["metros"]; ?></p>
                </div>
            </li>
            <?php endforeach;?>
        </ul>
    </div>

    <div class="row" id="mapa">
        <div class="twelve columns">
            <h3 class="titulo vermelho">Mapa</h3>
        </div>
        <div class="twelve columns">
            <p class="chamada">Confira no mapa todos os nossos empreendimentos.</p>
        </div>
    </div>

    <div class="row mapa">
        <div class="container-mapa">
            <div id="map-canvas"></div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>