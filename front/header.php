<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Grupo Pacaembu Construtora - Haus Construtora</title>
    <link rel="stylesheet" href="stylesheets/app.css">
    <link rel="stylesheet" href="stylesheets/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="stylesheets/fancybox/jquery.fancybox-buttons.css">
    <link rel="stylesheet" href="stylesheets/fancybox/jquery.fancybox-thumbs.css">
    <script src="javascripts/modernizr.foundation.js"></script>
</head>
<?php 
    $classAtual = "";

    $urlAtual = $_SERVER["PHP_SELF"];
    $pastas = Explode('/', $urlAtual);
    $arquivoAtual = $pastas[count($pastas) - 1];

    $telasGrandes = array('index.php', 'empreendimentos-detalhe.php', 'lancamentos-detalhe.php' );

    if ( in_array($arquivoAtual, $telasGrandes) ) {

        $classAtual = "bg-grande " . substr($arquivoAtual, 0, strlen($arquivoAtual) - 4);
    }
    ?>
<body class="<?php echo "$classAtual"; ?>">
    <div class="row">
        <div class="twentyfour columns menu-secundario">
            <dl class="sub-nav">
                <dd><a href="#webmail" class="webmail" ><span data-icon="&#xe000;"></span> WEBMAIL</a></dd>
                <dd><a href="#intranet" class="intranet"><span data-icon="&#xe001;"></span> INTRANET</a></dd>
                <dd><a href="trabalhe-conosco.php" class="trabalhe-conosco"><span data-icon="&#xe002;"></span> TRABALHE CONOSCO</a></dd>
                <dd><a href="fale-ouvidoria.php" class="trabalhe-conosco"><span data-icon="&#xe005;"></span> FALE COM A OUVIDORIA</a></dd>
            </dl>
        </div>
    </div>

    <div class="row header">
        <div class="eight columns">
            <h1><a href="index.php" class="logo">Grupo Pacaembu Construtora - Haus Construtora</a></h1>
        </div>
        <div class="sixteen columns menu-principal">
            <ul class="menu nav-bar">
                <li class="has-flyout">
                    <a href="empresa.php">empresa</a>
                    <ul class="sub-menu flyout">
                        <li><a href="empresa.php#missao">Missão e visão</a></li>
                        <li><a href="empresa.php#nossa-equipe">Nossa equipe</a></li>
                        <li><a href="empresa.php#certificados">Certificados</a></li>
                    </ul>
                </li>
                <li><a href="empreendimentos.php">empreendimentos</a></li>
                <li class="has-flyout"><a href="lancamentos.php">lançamentos</a>
                    <ul class="sub-menu flyout">
                        <li><a href="lancamentos-detalhe.php?id=1">São Rogério</a></li>
                        <li><a href="lancamentos-detalhe.php?id=2">Casa Branca</a></li>
                        <li><a href="lancamentos-detalhe.php?id=3">Novo Horizonte</a></li>
                    </ul>
                </li>
                <!-- <li><a href="clientes.php">clientes</a></li> -->
                <li><a href="fale-conosco.php">fale conosco</a></li>
            </ul>
        </div>
    </div>

<?php
$empreendimentos = array(
    '0' => array(
        'nome'       => 'Residencial da Lealdade e Amizade', 
        'cidade'     => 'S. J. do Rio Preto',
        'bairro'     => '',
        'endereco'   => 'Estrada Municipal SJRP, 351',
        'quartos'    => '2 dorms',
        'metros'     => 'Casas com 41,20m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao'   => '22',
        'unidades'   => '2.508',
        'telefone'   => '0800-7302020',
        'galeria'    => 25,
        'cameras'    => array(
            'rtmp://68.232.187.245:1935/p61-lealami1/p61-lealami1.stream',
            'rtmp://68.232.187.245:1935/p62-lealami2/p62-lealami2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '1' => array(
        'nome' => 'Residencial Flora Rica', 
        'cidade' => 'Botucatu',
        'bairro' => '',
        'endereco' => 'Rua Luiz Vizotto, 277',
        'quartos' => '2 dorms',
        'metros' => 'Casas com 45,64m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => '100',
        'unidades' => '211',
        'telefone' => '0800-7302020',
        'galeria' => 31,
        'cameras' => array(),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '2' => array(
        'nome' => 'Residencial Santo Expedito', 
        'cidade' => 'Ibitinga',
        'bairro' => 'Lapa',
        'endereco' => 'Rua 11 com IMG 020',
        'quartos' => '2 dorms',
        'metros' => '41,20m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => '7',
        'unidades' => '418',
        'telefone' => '0800-7302020',
        'galeria' => 14,
        'cameras' => array(
            'rtmp://68.232.187.245:1935/p68-ibitinga1/p68-ibitinga1.stream',
            'rtmp://68.232.187.245:1935/p69-ibitinga2/p69-ibitinga2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    ),
    '3' => array(
        'nome' => 'Residencial Jardim Bela Vista', 
        'cidade' => 'Lins',
        'bairro' => '',
        'endereco' => 'Rua Professor António Seabra, S/N',
        'quartos' => '2 dorms',
        'metros' => 'Área útil de 42,5 m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => '98',
        'unidades' => '486',
        'telefone' => '0800-7302020',
        'galeria' => 20,
        'cameras' => array(),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '4' => array(
        'nome' => 'Residencial Parque das Flores', 
        'cidade' => 'Mirassol',
        'bairro' => '',
        'endereco' => 'Rodovia Vicinal Mirassol a Ruilândia s/n – KM 01',
        'quartos' => '2 e 3 dorms',
        'metros' => '36.72 m² e 53,86 m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => array(
            'Mod. I' => '100',
            'Mod. II' => '99',
            'Mod. III' => '83',
            'Mod. IV' => '59'
        ),
        'unidades' => '1051',
        'telefone' => '0800-7302020',
        'galeria' => 20,
        'cameras' => array(
            'rtmp://68.232.187.245:1935/p14-Mirassol1/p14-Mirassol1.stream',
            'rtmp://68.232.187.245:1935/p15-Mirassol2/p15-Mirassol2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '5' => array(
        'nome' => 'Residencial São Sebastião', 
        'cidade' => 'Taquaritinga',
        'bairro' => '',
        'endereco' => 'Avenida Capitão José Camargo de Lima, S/N',
        'quartos' => '2 dorms',
        'metros' => '46,48 m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => '64',
        'unidades' => '112',
        'telefone' => '0800-7302020',
        'galeria' => 18,
        'cameras' => array(),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '6' => array(
        'nome' => 'Residencial Luz da Esperança', 
        'cidade' => 'S. J. do Rio Preto',
        'bairro' => '',
        'endereco' => 'Estrada Municipal Miguel Ferreira Ribeiro (Boiadeira), S/N',
        'quartos' => '2 dorms',
        'metros' => '36.72 m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => array(
            'Mod. I' => '89',
            'Mod. II' => '72',
            'Mod. III' => '54'
        ),
        'unidades' => '1038',
        'telefone' => '0800-7302020',
        'galeria' => 34,
        'cameras' => array(
            'rtmp://68.232.187.245:1935/p34-saojose1/p34-saojose1.stream',
            'rtmp://68.232.187.245:1935/p35-saojose2/p35-saojose2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '7' => array(
        'nome' => 'Residencial São Rogério', 
        'cidade' => 'Avaré',
        'bairro' => '',
        'endereco' => 'Av. Manoel Teixeira Sampaio, S/N',
        'quartos' => '2 dorms',
        'metros' => 'Casas com 43,74m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => array(
            'Mod. I' => '73',
            'Mod. II' => '53',
            'Mod. III' => '10'
        ),
        'unidades' => '370',
        'telefone' => '0800-7302020',
        'galeria' => 20,
        'cameras' => array(
            'rtmp://68.232.187.245:1935/p40-sao rogerio1/p40-sao rogerio1.stream',
            'rtmp://68.232.187.245:1935/p41-sao rogerio2/p41-sao rogerio2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    ), 
    '8' => array(
        'nome' => 'Residencial Jardim Itália', 
        'cidade' => 'Bebedouro',
        'bairro' => '',
        'endereco' => 'Rua José Minholo, S/N',
        'quartos' => '2 dorms',
        'metros' => 'Casas com 41,20m²',
        'descritivo' => '<p><strong>Área útil de 38,13m²</strong>, sendo:</p><ul><li><strong>Sala</strong>: 8,41m²; Cozinha: 8,07m²;</li><li><strong>Banheiro</strong>: 3,47m²;</li><li><strong>Dormitório-1</strong>: 7,54m²</li><li><strong>Dormitório-2</strong>: 9,36m²</li><li><strong>Hall</strong>: 1,82m².</li><li><strong>Área Total construída</strong>: 42,75m².</li><li><strong>Não Murada.</strong></li></ul>',
        'evolucao' => '10',
        'unidades' => '421',
        'telefone' => '0800-7302020',
        'galeria' => 20,
        'cameras' => array(
            'rtmp://68.232.187.245:1935/p65-jardimitalia1/p65-jardimitalia1.stream',
            'rtmp://68.232.187.245:1935/p66-jardimitalia2/p66-jardimitalia2.stream'
        ),
        'mostrar_em' => array('empreendimentos', 'obras')
    )
);


$lancamentos = array(
    array(
        'nome' => 'Residencial São Rogério', 
        'cidade' => 'Avaré',
        'endereco' => 'Rua Pará, 1365. próximo ao Mercado Municipal',
        'descritivo' => 'A <strong>Pacaembu</strong> tem orgulho de lançar em Avaré o Residencial São Rogério. São casas com 2 dormitórios, excelente acabamento e total infraestrutura para você e sua família',
        'textos_blocos' => 'Parcelas a partir de <strong class="vermelho">R$ 427,50</strong><br>
            Renda familiar mínima de <strong class="vermelho">R$ 1.430,00</strong><br>
            Subsídio de até <strong class="vermelho">R$ 11.000,00</strong><br>
            Preço de venda <strong class="vermelho">R$ 70.000,00</strong><br>
            Até 100% financiado pela Caixa Com possibilidade de Utilizar o FGTS',
        'blocos' => array(
            'Área total' => '42,75m²',
            'Medidas do Terreno' => '10 x 20m, total de 200m²',
            'Medidas das casas' => '<strong>Área útil 38,13m2</strong>, sendo<br><strong>Sala</strong>:8,41m2;<br><strong>Cozinha</strong>:8,07m2;<br><strong>Banheiro</strong>:3,47m2;<br><strong>Dormitório 1</strong>: 7,54m2;<br><strong>Dormitório 2</strong>: 9,36m2<br><strong>Hall</strong>: 1,82m2.<br><strong>Área Total construída (com paredes) 42,75m2</strong>',
        ),
        'banners' => array(
            'banner-inscricao1.jpg',
            'banner-minhacasa.jpg',
            'banner-sindicato.jpg',
            'banner-caixa.jpg'
        )
    ),
    array(
        'nome' => 'Residencial Casa Branca', 
        'cidade' => 'Casa Branca',
        'endereco' => 'Estrada Vicinal Giácomo Milan - S/N - Casa Branca',
        'descritivo' => 'A <strong>Pacaembu</strong> tem orgulho de lançar em Casa Branca o Residencial Nova Casa Branca. ão casas com 2 dormitórios, excelente acabamento e total infraestrutura para você e sua família.',
        'textos_blocos' => 'Parcelas a partir de <strong class="vermelho">R$ 468,00</strong><br>
            Renda familiar mínima de <strong class="vermelho">R$ 1.560,00</strong><br>
            Subsídio de até <strong class="vermelho">R$ 13.000,00</strong><br>
            Preço de venda <strong class="vermelho">R$ 77.900,00</strong><br>
            Até 100% financiado pela Caixa Com possibilidade de Utilizar o FGTS',
        'blocos' => array(
            'Área da Casa' => '43,74m²',
            'Área de Piso' => '37,74 m²',
            'Medidas do Terreno' => '9,50m x 18m, total de 171 m²',
        ),
        'banners' => array(
            'banner-inscricao2.jpg',
            'banner-alje.jpg',
            'banner-prefeitura.jpg',
            'banner-caixa.jpg',
            'banner-minhacasa.jpg'
        )
    ),
    array(
        'nome' => 'Residencial Novo Horizonte', 
        'cidade' => 'Novo Horizonte',
        'endereco' => 'Estrada Vicinal Giácomo Milan - S/N - Casa Branca',
        'descritivo' => 'A <strong>Pacaembu</strong> tem orgulho de lançar em Novo Horizonte o Residencial Jardim das Oliveiras . São casas com 2 dormitórios, excelente acabamento e total Infraestrutura para você e sua família.',
        'textos_blocos' => 'Parcelas a partir de <strong class="vermelho">R$ 457,18</strong><br>
            Renda familiar mínima de <strong class="vermelho">R$ 1.530,00</strong><br>
            Subsídio de até <strong class="vermelho">R$ 11.621,00</strong><br>
            Preço de venda <strong class="vermelho">R$ 74.900,00</strong><br>
            Até 100% financiado pela Caixa Com possibilidade de Utilizar o FGTS',
        'blocos' => array(
            'Área total construída' => '42,75m²',
            'Medidas do Terreno' => '8 x 20 m, total de 160 m²',
            'Medidas da Casa' => '<strong>Área útil</strong> 38,13 m2, sendo<br>
                <strong>Sala</strong>: 8,41 m2;<br>
                <strong>Cozinha</strong>: 8,07 m2;<br>
                <strong>Banheiro</strong>:3,47 m2;<br>
                <strong>Dormitório 1</strong>: 7,54 m2;<br>
                <strong>Dormitório 2</strong>: 9,36 m2<br>
                <strong>Hall</strong>: 1,82 m2.<br>
                <strong>Área Total construída (com paredes) 42, 75 m2</strong>',
        ),
        'banners' => array(
            'banner-inscricao3.jpg',
            'banner-caixa.jpg',
            'banner-minhacasa.jpg'
        )
    ),
);

function ordenaPorNome($a, $b) {
    return strcasecmp( $a['nome'], $b['nome']);
}

uasort($empreendimentos, 'ordenaPorNome');

?>