;(function ($, window, undefined) {
    'use strict';

    var $doc = $(document),
    Modernizr = window.Modernizr;

    $(document).ready(function() {
        $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
        $.fn.foundationButtons          ? $doc.foundationButtons() : null;
        $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
        $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
        $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
        $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
        $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
        $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
        $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
        $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
        $.fn.foundationClearing         ? $doc.foundationClearing() : null;

        $.fn.placeholder                ? $('input, textarea').placeholder() : null;
    });

    $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
    $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
    $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
    $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

    // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
    if (Modernizr.touch && !window.location.hash) {
        $(window).load(function () {
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        });
    }
})(jQuery, this);

function inicializaMapa() {
    // if( ! window.google ) return;

    var marker;
    var map;
    var SaoPaulo = new google.maps.LatLng(-22.177232,-47.845459);
    var posicoesEmpreendimentos = [
        new google.maps.LatLng(-23.548881,-46.638336),
        new google.maps.LatLng(-23.682804,-46.595546),
        new google.maps.LatLng(-20.820181,-49.379681),
        new google.maps.LatLng(-22.983134,-49.857104),
        new google.maps.LatLng(-20.737811,-48.910746),
        new google.maps.LatLng(-22.413399,-47.569574),
        new google.maps.LatLng(-23.104426,-48.940060)
    ];

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: SaoPaulo
    });

    for (var i = posicoesEmpreendimentos.length - 1; i >= 0; i--) {
        marker = new google.maps.Marker({
            map:map,
            position: posicoesEmpreendimentos[i],
            icon: 'images/sprites/casinha-logo.png'
        });
    };
}

function inicializarPlayer() {
    flowplayer("a.camera-player", {
        src:"swf/flowplayer.commercial-3.2.7.swf",
        wmode: "opaque" // This allows the HTML to hide the flash content
    }, {
    
        key: '#$b0f01c1e0587e75e5da',
        logo: {
            url: '../images/logo_player.png',
            fullscreenOnly: false
        },
        clip:{
            autoPlay: true,
            bufferLength: 6,
            onBeforeFinish: function() { return false; }
        },
        plugins:  {
            controls:  {
                volume: false       
            },
            rtmp: {
            url: 'swf/flowplayer.rtmp-3.2.3.swf'
            }
        }
    });
}

$(window).load(function() {
    if( $('#map-canvas').length ) inicializaMapa();
    if( $('a.camera-player').length ) inicializarPlayer();

     $('.index .container-destaques').orbit({ 
        fluid: '16x6',
        animation: 'horizontal-push',
        animationSpeed: 800,
        advanceSpeed: 5000,
        // startClockOnMouseOut: true,
        captions: true,
        captionAnimation: 'slideOpen',
        bullets: true
    });
});

$(document).ready(function() {
    // Menu
    $menuItem = $("ul.menu > li");
    $menuItem.on("mouseenter mouseleave", function(ev){
        var animaSpeed = 300;
        var $self = $(this);
        var $link = $self.find('> a:eq(0)');
        var $submenu = $link.next();
        if(ev.type == "mouseenter")
        {
            $link.animate({backgroundColor: "#d21000"}, animaSpeed);
            $self.animate({backgroundColor: "#d21000"}, animaSpeed);
            $submenu.slideDown({queue: false, duration: animaSpeed});
        }
        else
        {
            $submenu.slideUp({queue: false, duration: animaSpeed});
            animateMenu($link, $self, 100);
        }
    });

    function animateMenu(link, self, animaSpeed)
    {
        // IE
        if(jQuery.support.opacity)
        {
            link.animate({backgroundColor: "transparent"}, animaSpeed);
            self.animate({backgroundColor: "transparent"}, animaSpeed);
        }
        else{
            link.removeAttr('style');
            self.removeAttr('style');
        }
    }

    $(".fancybox:not([rel='camera-group'])").fancybox({
        padding: 0,
        helpers : {
            thumbs  : {
                width  : 80,
                height : 80
            }
        }
    });

    $(".fancybox[rel='camera-group']").fancybox({
        padding: 0,
        margin: 0,
        width: 710,
        height: 540,
        autoScale: false
    });

    $('input.file').change(function (e) {
        e.preventDefault();
        var valor = $(this).val();
        var arquivo = valor.split("\\");
        if(arquivo.length <= 0){
            arquivo = valor.split("/");
        }
        arquivo = arquivo[arquivo.length-1];
        $(this).siblings('.fakefile').find('input[type="text"]').val(arquivo);
    });

    $('.telefone-mascara').mask("(99) 9999-9999?9");
});