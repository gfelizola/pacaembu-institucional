<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twentyfour columns">
            <div class="row">
                <div class="twentyfour columns">
                    <h3 class="titulo preto">Fale com a Ouvidoria</h3>
                    <p><strong>Canais de atendimento</strong></p>
                    <p><strong>E-mail:</strong> <a href="mailto:ouvidoria@pacaembu.com.br">ouvidoria@pacaembu.com.br</a></p>
                    <p><strong>Telelfone:</strong> 0800 – 326-0990</p>
                    <p><strong>Carta:</strong><br>
                        Ouvidoria Pacaembu<br>
                        Rua Sabará, 566, 22º andar –cj. 223 – Higienópolis<br>
                        São Paulo – SP - CEP 01239-010</p>
                </div>
            </div>
            <div class="row subarea">
                <div class="twentyfour columns">
                    <form action="#" class="linha-sobre">
                        <div class="row subarea">
                            <div class="twelve columns">
                                <label>Nome Completo:</label>
                                <input type="text" />
                            </div>
                            <div class="twelve columns">
                                <label>E-mail:</label>
                                <input type="text" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="twentyfour columns">
                                <label>Telefone:</label>
                                <input type="text" name="telefone" id="telefone" class="telefone-mascara" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="twelve columns">
                                <label>Município:</label>
                                <input type="text" />
                            </div>
                            <div class="twelve columns">
                                <label>Obra/Escritório:</label>
                                <input type="text" />
                            </div>
                        </div>

                        <div class="row">
                        </div>

                        <div class="row">
                            <div class="twentyfour columns">
                                <label>Assunto:</label>
                                <input type="text" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="twentyfour columns">
                                <label>Mensagem</label>
                                <textarea name="carta" id="carta" cols="30" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="twentyfour columns">
                                <input type="submit" value="Enviar" class="btn-enviar titulo vermelho" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>