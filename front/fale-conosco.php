<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="sixteen columns">
            <h3 class="titulo preto">Fale Conosco</h3>

            <form action="#" class="linha-sob">
                <div class="row">
                    <div class="twentyfour columns">
                        <label>Nome:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>E-mail:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>Telefone:</label>
                        <input type="text" name="telefone" id="telefone" class="telefone-mascara" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>Assunto:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>Mensagem</label>
                        <textarea name="carta" id="carta" cols="30" rows="5"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <input type="submit" value="Enviar" class="btn-enviar titulo vermelho" />
                    </div>
                </div>
            </form>
        </div>

        <div class="eight columns">
            <p><a href="empreendimentos.html"><img src="images/destaque-catho.jpg" /></a></p>
            <p><a href="nossas-vagas.html"><img src="images/chamada-nossas-vagas.jpg" ></a></p>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>