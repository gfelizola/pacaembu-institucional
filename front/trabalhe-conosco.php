<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="sixteen columns">
            <h3 class="titulo preto">Trabalhe Conosco</h3>

            <p>O Grupo Pacaembu está em um crescimento contínuo, o que gera oportunidades, desafios e resultados positivos.</p>
            <p>O segredo do nosso sucesso é a paixão em que cada colaborador coloca nas soluções inovadoras que desenvolve sempre com qualidade. O seu talento pode nos ajudar a continuar no topo. Para isso, contamos com a dedicação dos melhores profissionais do mercado. Se você se identifica com o perfil da Pacaembu, com nossa missão e valores, sempre buscando a excelência, envie seu currículo.</p>
            <p>Cadastre seu currículo e construa conosco sua carreira!</p>

            <form action="#" class="linha-sob">
                <div class="row">
                    <div class="twentyfour columns">
                        <label>cargo pretendido</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>carta de apresentação</label>
                        <textarea name="carta" id="carta" cols="30" rows="5"></textarea>
                    </div>
                </div>
                
                <div class="row">
                    <div class="twentyfour columns">
                        <label>seu currículo</label>
                        <div class="row collapse">
                            <div class="two columns">
                                <span class="prefix"><img src="images/sprites/seta-vermelha-arquivo.png" /></span>
                            </div>
                            <div class="twentytwo columns fileinputs">
                                
                                    <input type="file" class="file" />
                                    <div class="fakefile">
                                        <input type="text" placeholder="ESCOLHER ARQUIVO" />
                                    </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>seu nome</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>seu e-mail</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <input type="submit" value="Enviar" class="btn-enviar titulo vermelho" />
                    </div>
                </div>
            </form>
        </div>

        <div class="eight columns">
            <p><a href="empreendimentos.html"><img src="images/destaque-catho.jpg" /></a></p>
            <p><a href="nossas-vagas.html"><img src="images/chamada-nossas-vagas.jpg" ></a></p>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>