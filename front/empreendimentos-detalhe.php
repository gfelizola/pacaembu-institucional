<?php include 'header.php'; ?>
<?php 
    if( isset($_GET['id']) ){
        $id = $_GET['id'];
        $emp = $empreendimentos[$id - 1];
    } else {
        header( 'Location: empreendimentos.php' ) ;
    }
?>
<div class="conteudo">
    <div class="row destaques">
        <div class="twentyfour columns">
            <div class="container-destaques">
                <img src="images/empreendimento-destaque<?php echo "$id"; ?>.jpg" alt="">
                <div class="descritivo">
                    <h3 class="titulo branco">Descritivo</h3>
                    <?php echo $emp['descritivo']; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row subarea">
        <div class="eight columns">
            <h3 class="titulo vermelho linha-sob">Galeria</h3>
            <div class="lista-galeria">
                <ul class="block-grid two-up">
                    <?php 
                        $max = 6;
                        for ($i=1; $i <= $emp['galeria']; $i++) { 
                            $class = "";
                            if( $i > $max ) $class= "hidden";

                            echo "<li class='$class'><a rel='galeria-group' class='fancybox' href='images/galerias/emp$id/imagem$i.jpg'><img src='images/galerias/emp$id/imagem$i.jpg' /></a></li>";
                        }
                    ?>
                </ul>
            </div>
            <div class="linha-sobre linha-sob obras">
                <p><strong>evolução da obra</strong></p>
                <ul>
                    <?php if( is_array($emp['evolucao']) ){
                        foreach ($emp['evolucao'] as $mod => $evo) {
                            ?>
                            <li>
                                <div class="porcentagem">
                                    <div class="valor p<?php echo $evo ?>"><?php echo $evo ?>%</div>
                                </div>
                                <span><?php echo $mod ?></span>
                            </li>
                            <?php
                        }
                    } else {
                        ?>
                        <li>
                            <div class="porcentagem">
                                <div class="valor p<?php echo $emp['evolucao'] ?>"><?php echo $emp['evolucao'] ?>%</div>
                            </div>
                        </li>
                        <?php
                    } ?>
                </ul>
            </div>
        </div>
        <div class="eight columns">
            <h3 class="titulo vermelho linha-sob">Mapa</h3>
            <p class="vermelho"><strong><?php echo $emp['cidade'] ?></strong></p>
            <div class="mapa">
                <div class="container-mapa">
                    <div id="map-canvas"></div>
                </div>
            </div>
            <p>Endereco: <strong><?php echo $emp['endereco'] ?></strong><br>
                Total de unidades: <strong><?php echo $emp['unidades'] ?></strong><br>
                Telefone para contato: <strong><?php echo $emp['telefone'] ?></strong>
            </p>
        </div>
        <div class="eight columns">
            <h3 class="titulo vermelho linha-sob">Câmeras Online</h3>
            <p>Visualize em tempo real a obra através de câmeras conectadas on line 24hs.</p>
            <?php 
            $i = 1 ;
            foreach ($emp['cameras'] as $camera) {
                ?>
                <a href="<?php echo $camera ?>" class="camera-player link-lupa fancybox fancybox.iframe" rel="camera-group"></a>
                <!-- <a href="<?php echo $camera['grande'] ?>" > -->
                    <!-- <img src="images/thumb_camera<?php echo $i ?>.jpg" /> -->
                    <!-- <iframe src="<?php echo $camera['miniatura'] ?>" frameborder="0" width="320" height="240" scrolling="no"></iframe> -->
                    <!-- <span class="lupa">ver mais</span> -->
                <!-- </a> -->
                <?php
                $i ++ ;
                if( $i > 2 ) $i = 1 ;
            }
            ?>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>