<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twentyfour columns">
            <h3 class="titulo preto">Venda seu terreno</h3>

            <p>O Grupo Pacaembu com o seu crescimento continuo quer expandir cada vez mais e levar para todo o Brasil seu compromisso com projetos habitacionais. Se você é proprietário de um terreno e tem interesse em negociá-lo, preencha o formulário abaixo e aguarde nosso contato</p>

            <form action="#" class="linha-sobre custom">
                <h4 class="titulo vermelho">Informações sobre você</h4>

                <div class="row subarea">
                    <div class="twelve columns">
                        <label>Nome Completo:</label>
                        <input type="text" />
                    </div>
                    <div class="twelve columns">
                        <label>E-mail:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twelve columns">
                        <label>Telefone:</label>
                        <input type="text" name="telefone" id="telefone" class="telefone-mascara" />
                    </div>
                    <div class="twelve columns">
                        <label>Celular:</label>
                        <input type="text" name="celular" id="celular" class="telefone-mascara" />
                    </div>
                </div>

                <div class="row">
                    <div class="five columns">
                        <label>Relação com o terreno:</label>
                        <select name="relacao" id="relacao" style="display:none; width:100%;">
                            <option value="">Proprietário</option>
                            <option value="">Corretor</option>
                            <option value="">Indicação</option>
                        </select>
                    </div>

                    <div class="nineteen columns">
                        <label>Empresa:</label>
                        <input type="text" />
                    </div>
                </div>

                <h4 class="titulo vermelho subarea">Informações sobre o terreno</h4>

                <div class="row">
                    <div class="six columns">
                        <label>Estado:</label>

                        <?php $estados = array(
                            array( 'nome' => 'ACRE',                'sigla' => 'AC' ),
                            array( 'nome' => 'ALAGOAS',             'sigla' => 'AL' ),
                            array( 'nome' => 'AMAZONAS',            'sigla' => 'AM' ),
                            array( 'nome' => 'AMAPÁ',               'sigla' => 'AP' ),
                            array( 'nome' => 'BAHIA',               'sigla' => 'BA' ),
                            array( 'nome' => 'CEARÁ',               'sigla' => 'CE' ),
                            array( 'nome' => 'DISTRITO FEDERAL',    'sigla' => 'DF' ),
                            array( 'nome' => 'ESPIRITO SANTO',      'sigla' => 'ES' ),
                            array( 'nome' => 'GOIÁS',               'sigla' => 'GO' ),
                            array( 'nome' => 'MARANHÃO',            'sigla' => 'MA' ),
                            array( 'nome' => 'MINAS GERAIS',        'sigla' => 'MG' ),
                            array( 'nome' => 'MATO GROSSO DO SUL',  'sigla' => 'MS' ),
                            array( 'nome' => 'MATO GROSSO',         'sigla' => 'MT' ),
                            array( 'nome' => 'PARA',                'sigla' => 'PA' ),
                            array( 'nome' => 'PARAÍBA',             'sigla' => 'PB' ),
                            array( 'nome' => 'PERNAMBUCO',          'sigla' => 'PE' ),
                            array( 'nome' => 'PIAUÍ',               'sigla' => 'PI' ),
                            array( 'nome' => 'PARANÁ',              'sigla' => 'PR' ),
                            array( 'nome' => 'RIO DE JANEIRO',      'sigla' => 'RJ' ),
                            array( 'nome' => 'RIO GRANDE DO NORTE', 'sigla' => 'RN' ),
                            array( 'nome' => 'RONDÔNIA',            'sigla' => 'RO' ),
                            array( 'nome' => 'RORAIMA',             'sigla' => 'RR' ),
                            array( 'nome' => 'RIO GRANDE DO SUL',   'sigla' => 'RS' ),
                            array( 'nome' => 'SANTA CATARINA',      'sigla' => 'SC' ),
                            array( 'nome' => 'SERGIPE',             'sigla' => 'SE' ),
                            array( 'nome' => 'SÃO PAULO',           'sigla' => 'SP' ),
                            array( 'nome' => 'TOCANTINS',           'sigla' => 'TO' )
                        ); ?>

                        <select name="estado" id="estado">
                            <?php 
                            foreach ($estados as $uf) {
                                echo "<option>" . $uf['sigla'] . " - " . $uf['nome'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="eighteen columns">
                        <label>Cidade:</label>
                        <input type="text" name="celular" id="celular" class="telefone-mascara" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>Endereço:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="eighteen columns">
                        <label>Bairro:</label>
                        <input type="text" />
                    </div>
                    <div class="six columns">
                        <label>Área (em m²):</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="eight columns">
                        <label>CEP:</label>
                        <input type="text" />
                    </div>
                    <div class="sixteen columns">
                        <label>Ponto de referencia:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="eight columns">
                        <label>Nº Contribuinte:</label>
                        <input type="text" />
                    </div>
                    <div class="sixteen columns">
                        <label>Link com fotos:</label>
                        <input type="text" />
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <label>Proposta</label>
                        <textarea name="proposta" id="proposta" cols="30" rows="5"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="twentyfour columns">
                        <input type="submit" value="Enviar" class="btn-enviar titulo vermelho" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>