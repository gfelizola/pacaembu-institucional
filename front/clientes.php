<?php include 'header.php'; ?>
<div class="conteudo">
    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Clienter públicos</h3>
        </div>
        <div class="twelve columns">
            <ul class="breadcrumbs">
                <li><a href="#">lista completa</a></li>
            </ul>
        </div>
    </div>

    <div class="row listagem-items">
        <ul>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente A</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente B</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente C</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente D</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente E</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente F</a></h4>
                </div>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Clienter privados</h3>
        </div>
        <div class="twelve columns">
            <ul class="breadcrumbs">
                <li><a href="#">lista completa</a></li>
            </ul>
        </div>
    </div>

    <div class="row listagem-items">
        <ul>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente A</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente B</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente C</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente D</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente E</a></h4>
                </div>
            </li>
            <li class="media">
                <div class="img linha-sobre">
                    <a href="#" class="container-img">
                        <img src="images/cliente_fake1.jpg">
                    </a>
                </div>
                <div class="body">
                    <h4><a href="#">Cliente F</a></h4>
                </div>
            </li>
        </ul>
    </div>
</div>
<?php include 'footer.php'; ?>