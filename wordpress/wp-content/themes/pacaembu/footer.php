<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>
	</div><!-- #main .wrapper -->

	<div class="footer-unidades">
        <div class="row">
            <div class="six columns">
                <p class="text-center">
                    <strong>Confira nossas publicações</strong>
                    <?php
                        $publicacoes = new WP_Query( array(
                            'post_type'      => 'publicacoes',
                            'posts_per_page' => 1
                        ));

                        if( $publicacoes->have_posts() ){
                            $publicacoes->the_post();

                            // var_dump($post);
                            $capa = get_field('capa');

                            echo "<a href=\"/publicacoes\" class=\"th\">
                                <img src=\"" . $capa['sizes']['medium'] . "\"  />
                            </a>";
                        }
                        
                        wp_reset_postdata();
                    ?>
                </p>
                
                <!-- <img src="<?php bloginfo('template_url') ?>/images/icone-ipad.png" />
                <p>Este site é acessível<br>
                    via <strong>iPad</strong> e outros<br>
                    dispositivos mobile</p> -->
            </div>
            <div class="seventeen columns unidades">
                <div class="row cima">
                <?php
                $unidades = get_field('unidades','options');
                $c = 0;
                foreach ($unidades as $unid) {
                    ?>
                    <div class="twelve columns unidade">
                    <h3><?php echo $unid['cidade'] ?> <small><?php echo $unid['estado'] ?></small></h3>
                    <p><?php echo $unid['endereco'] ?><br>
                        <?php echo $unid['bairro'] ?> - CEP <?php echo $unid['cep'] ?><br>
                        <?php echo $unid['telefone'] ?></p>
                    </div>
                    <?php
                    $c++;
                    if( $c >= 2 ){
                        $c = 0;
                        echo "</div><div class='row'>";
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="row">
            <?php wp_nav_menu( array( 
                'theme_location' => 'SiteMap', 
                'container_class' => 'nineteen columns menu-sitemap',
                'menu_class' => 'the_listmap' ) ); ?>
            <div class="five columns">
                <p class="branco">Conheça nossas redes sociais</p>
                <ul class="social">
                    <li><a href="https://www.facebook.com/pacaembuconstrutora" target="_blank" class="fb">Facebook</a></li>
                    <li><a href="https://twitter.com/pacaembuconstru" target="_blank" class="tw">Twitter</a></li>
                    <li><a href="http://www.linkedin.com/company/2996730" target="_blank" class="in">LinkedIn</a></li>
                    <li><a href="http://www.youtube.com/user/PacaembuConstrutora" target="_blank" class="yt">YouTube</a></li>
                </ul>
            </div>
        </div>
        <div class="row direitos cf">
            <p><?php echo date("Y"); ?>© Grupo Pacaembu Construtora - Todos os direitos reservados</p>
        </div>
    </div>

    <div class="barra-contato">
        <div class="row">
            <div class="two mobile-one column logo">
                <a href="<?php echo site_url()?>">Página Inicial</a>
            </div>
            <div class="fourteen mobile-three columns alo-pacaembu">
                <h3>Alô Pacaembu - <span>0800 730 2020</span></h3>
            </div>

            <div class="four mobile-two columns menu-secundario">
                <dl class="sub-nav">
                    <dd><a href="<?php echo site_url() ?>/fale-conosco/" class="fale-conosco"><span data-icon="&#xe008;"></span> FALE CONOSCO</a></dd>
                </dl>
            </div>

            <div class="four mobile-two columns social-container">
                <ul class="social">
                    <li><a href="https://www.facebook.com/pacaembuconstrutora" target="_blank" class="fb">Facebook</a></li>
                    <li><a href="https://twitter.com/pacaembuconstru" target="_blank" class="tw">Twitter</a></li>
                    <li><a href="http://www.linkedin.com/company/2996730" target="_blank" class="in">LinkedIn</a></li>
                    <li><a href="http://www.youtube.com/user/PacaembuConstrutora" target="_blank" class="yt">YouTube</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/foundation.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/jquery.foundation.forms.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/jquery.fancybox-thumbs.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/jquery.inview.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/gmaps-infobox.js"></script>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<script>
var url_template = "<?php bloginfo( 'template_url' ); ?>";
if(posicoesEmpreendimentos == undefined){
    var posicoesEmpreendimentos = [
    <?php
    wp_reset_query();
    $empreendimentos = new WP_Query( array(
        "post_type" => "empreendimentos",
        "posts_per_page" => -1
    ));

    if( $empreendimentos->have_posts() ){
        while ($empreendimentos->have_posts()) {
            $empreendimentos->the_post();
            $local = get_field('localizacao');
            
            // echo "new google.maps.LatLng(" . $local['coordinates'] . ")," ;

            $total_evolucao = 0;
            $evolucoes = get_field('evolucao');

            if( $evolucoes ){
                foreach ($evolucoes as $mod) {
                    $total_evolucao += $mod['percentual'];
                }

                $media_evolucao = round( $total_evolucao / count( $evolucoes ) );
            }
            
            if ( $local['coordinates'] && $media_evolucao < 99 ) {
                $logo = get_field('logo');
                $descricao = "<p><strong>" . get_field("cidade") . "</strong><br>" . get_field("dorms") ." <br>" . get_field("area") . "</p>";
                echo "{ nome: '" . get_the_title() . "', local: new google.maps.LatLng(" . $local['coordinates'] . "), logo: '" . $logo['url'] . "', descricao: '$descricao' },\n";
            };
        }
    }
    ?>
    ];
}
</script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/app.js"></script>

<?php wp_footer(); ?>
</body>
</html>