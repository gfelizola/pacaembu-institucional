<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>
<div class="conteudo">
	<div class="row">
		<div class="entry-header twentyfour columns">
			<?php the_breadcrumb(); ?>
		</div>
	</div>
	<div class="row">
		<div class="twentyfour columns">
			<?php if ( have_posts() ) : ?>
			<ul>
				<?php
				while ( have_posts() ) : the_post();
				get_template_part( 'content', 'lista' );
				endwhile;
				?>
			</ul><?php
			else :
				get_template_part( 'content', 'none' );
			endif;
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>