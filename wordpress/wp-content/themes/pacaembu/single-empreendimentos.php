<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

<div class="empreendimentos-detalhe">
    <div class="row">
        <div class="twentyfour columns">
            <?php the_breadcrumb(); ?>
        </div>
    </div>

    <div class="row destaques">
        <div class="twentyfour columns">
            <div class="container-destaques">
                <?php $destaque = get_field('destaque'); ?>
                <img src="<?php echo $destaque['url']; ?>" alt="">
                <div class="descritivo">
                    <h3 class="titulo branco">Descritivo</h3>
                    <?php the_field('descritivo') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row linha-sob">
        <div class="twelve columns">
            <h3 class="titulo vermelho">Plantas</h3>
            <div class="lista-galeria jcarousel-half">
                <ul>
                    <?php
                        $galeria = get_field('plantas');
                        foreach ($galeria as $img) {
                            $data_atualizacao = get_post_modified_time( 'j F Y', false, $img['id'], true );
                            echo "<li><a rel='plantas-group' title='" . $img['caption'] . "' class='fancybox' href='" . $img['url'] . "'><img src='" . $img['sizes']['thumbnail'] . "' /></a></li>";
                        }
                    ?>
                </ul>
            </div>
        </div>

        <div class="twelve columns">
            <h3 class="titulo vermelho">Galeria</h3>
            <div class="lista-galeria jcarousel-half">
                <ul>
                    <?php
                        $galeria = get_field('galeria');
                        $galeria_sort = array();

                        foreach ($galeria as $key => $gal) {
                            $data_atualizacao = get_the_time( 'Ymdhms', $gal['id'] );
                            $galeria_sort[$key] = $data_atualizacao;
                        }

                        array_multisort($galeria_sort, SORT_DESC, $galeria);

                        foreach ($galeria as $img) {
                            $data_atualizacao = get_post_modified_time( 'j F Y', false, $img['id'], true );
                            echo "<li><a rel='galeria-group' title='Foto publicada em: $data_atualizacao' class='fancybox' href='" . $img['url'] . "'><img src='" . $img['sizes']['thumbnail'] . "' /></a></li>\n";
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="row subarea linha-sob">
        <div class="eight columns">
            <div class="obras">
                <!-- <p><strong>evolução da obra</strong></p> -->
                <h3 class="titulo vermelho">Evolução da obra</h3>
                <ul>
                    <?php
                    $evolucoes = get_field('evolucao');

                    foreach ($evolucoes as $mod) {
                        ?>
                        <li>
                            <span><?php echo $mod['nome_modulo'] ?></span>
                            <div class="porcentagem">
                                <div class="valor p<?php echo $mod['percentual'] ?>">
                                    <div><?php echo $mod['percentual'] ?>%</div>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="sixteen columns">
            <h3 class="titulo vermelho">Câmeras Online</h3>
            <?php
            if ( get_field('sem_cameras') == false ) {
                $cameras = get_field('cameras');
            ?>
            <div class="row">
                <?php
                foreach ($cameras as $camera) {
                    $url_camera = $camera['url'];
                    if ($detect->isMobile()) $url_camera = $camera['url_mobile'];
                    ?>
                <div class="twelve columns">
                    <iframe src="<?php echo $url_camera ?>" frameborder="0" width="300" height="240" scrolling="no"></iframe>
                    <a href="<?php echo $url_camera ?>" class="button radius secondary fancybox fancybox.iframe">Ampliar</a>
                </div>
                <?php }
            ?></div><?php
            } else {
                echo "<p>Câmera indisponível no momento</p>";
            }
            ?>

        </div>
    </div>

    <div class="row subarea">
        <div class="twentyfour columns">
            <h3 class="titulo vermelho">Mapa</h3>
            <div class="mapa">
                <?php $local = get_field('localizacao'); ?>
                <div class="container-mapa">
                    <div id="map-canvas" data-localizacao="<?php echo $local['coordinates'] ?>"></div>
                </div>
            </div>
            <p><strong class="vermelho"><?php the_field('cidade') ?></strong><br>
                Endereço: <strong><?php the_field('endereco') ?></strong><br>
                Total de unidades: <strong><?php the_field('unidades') ?></strong><br>
                Telefone para contato: <strong><?php the_field('telefone') ?></strong>
            </p>
        </div>
    </div>
</div>
<?php get_footer(); ?>