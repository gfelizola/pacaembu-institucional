<?php
/**
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

/**
 * Sets up the content width value based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 960;

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Pacaembu supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Pacaembu 1.0
 */
function pacaembu_setup() {
	load_theme_textdomain( 'pacaembu', get_template_directory() . '/languages' );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 940, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'pacaembu_setup' );

function pacaembu_init(){
	register_nav_menus( array(
		'Principal' => __('Principal'), 
		'SiteMap' => __('SiteMap')
       ));
}
add_action( 'init', 'pacaembu_init' );

/**
 * Registers our main widget area and the front page widget areas.
 *
 * @since Pacaembu 1.0
 */
function pacaembu_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'pacaembu' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'pacaembu' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
       ) );
}
add_action( 'widgets_init', 'pacaembu_widgets_init' );

/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 * @since pacaembu 1.0
 */
function pacaembu_entry_meta() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'pacaembu' ) );

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'pacaembu' ) );

	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
       );

	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'Ver todos os posts de %s', 'pacaembu' ), get_the_author() ) ),
		get_the_author()
       );

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
	if ( $tag_list ) {
		$utility_text = __( 'Criado em %1$s e taggeado como %2$s em %3$s<span class="by-author"> por %4$s</span>.', 'pacaembu' );
	} elseif ( $categories_list ) {
		$utility_text = __( 'Criado em %1$s on %3$s<span class="by-author"> por %4$s</span>.', 'pacaembu' );
	} else {
		$utility_text = __( 'Criado em %3$s<span class="by-author"> por %4$s</span>.', 'pacaembu' );
	}

	printf(
		$utility_text,
		$categories_list,
		$tag_list,
		$date,
		$author
       );
}

/**
 * Extends the default WordPress body class to denote:
 * 1. Using a full-width layout, when no active widgets in the sidebar
 *    or full-width template.
 * 2. Front Page template: thumbnail in use and number of sidebars for
 *    widget areas.
 * 3. White or empty background color to change the layout and spacing.
 * 4. Custom fonts enabled.
 * 5. Single or multiple authors.
 *
 * @since pacaembu 1.0
 *
 * @param array Existing class values.
 * @return array Filtered class values.
 */
function pacaembu_body_class( $classes ) {
	$background_color = get_background_color();

	if ( ! is_active_sidebar( 'sidebar-1' ) 
		|| is_page_template( 'page-templates/empreendimentos.php' ) 
		|| is_page_template( 'page-templates/lancamentos.php' ) 
		)
		$classes[] = 'bg-grande';

	if ( is_page_template( 'page-templates/front-page.php' ) ) {
		$classes[] = 'template-front-page';
	}

	return $classes;
}
add_filter( 'body_class', 'pacaembu_body_class' );

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @since Pacaembu 1.0
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 * @return void
 */
function pacaembu_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}
add_action( 'customize_register', 'pacaembu_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since Pacaembu 1.0
 */
function pacaembu_customize_preview_js() {
	wp_enqueue_script( 'pacaembu_setup-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20120827', true );
}
add_action( 'customize_preview_init', 'pacaembu_customize_preview_js' );

/**
 * Função para impressão de breadcrumb
 *
 * @since Pacaembu 1.0
 */
function the_breadcrumb() {
	global $post;

    $pacaembu_cpts = array('empreendimentos','lancamentos','releases','materias','eventos');
    $pacaembu_cptn = array(
        'empreendimentos' => 'Empreendimentos' , 
        'lancamentos' => 'Lançamentos', 
        'releases' => 'Releases',
        'materias' => 'Notícias',
        'eventos' => 'Eventos'
    );

	echo '<ul class="breadcrumbs">';
	if (!is_home()) {
		echo '<li><a href="';
		echo get_option('home');
		echo '">';
		echo 'Página Inicial';
		echo "</a></li>";

		if (is_category()) {
			echo '<li>';
			the_category(' </li><li> ');
		} 
        elseif (is_singular($pacaembu_cpts)) 
        {
            $tipo = get_post_type_object( $post->post_type );

            $output = '<li><a href="';
            $output .= site_url( $post->post_type . '/' ); 
            $output .= '">';
            $output .= $tipo->label;
            $output .= "</a></li>";
            echo $output;
		} 
        elseif (is_page()) 
        {
			if($post->post_parent){ 
				$anc = get_post_ancestors( $post->ID );

				foreach ( $anc as $ancestor ) {
					$output = '<li><a href="';
					$output .= get_permalink($ancestor); 
					$output .= '">';
					$output .= get_the_title($ancestor);
					$output .= "</a></li>";
					echo $output;
				}
			}
		}

        if ( ! is_archive() ) {
            echo '<li class="current"><a href="' . get_permalink($post->ID) . '">';
            echo the_title();
            echo '</a></li>';
        } else {
            if( $post ){
                $tipo = get_post_type_object( $post->post_type );
                $output = '<li><a href="';
                $output .= site_url( $post->post_type . '/' ); 
                $output .= '">';
                $output .= $tipo->label;
                $output .= "</a></li>";
                echo $output;
            }
        }
	}
	// elseif (is_tag()) {single_tag_title();}
	// elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
	// elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
	// elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
	// elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
	// elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
	// elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
	
	
	
	echo '</ul>';
}

/**
 * Shortcode para ser usado com o Zurb Foundation 3.2.5
 *
 * @since Pacaembu 1.0
 */
function tag_zurb_row($atts, $content = ''){
	extract( shortcode_atts(array(
		'row_id' => ''
       ), $atts));
	echo "<div class='row' id='{$row_id}'>\n";
}
add_shortcode('zrow', 'tag_zurb_row');

function tag_zurb_columns($atts, $content = ''){
	extract( shortcode_atts(array(
		'row' => 'true',
		'cols' => 'twentyfour',
		'col_id' => ''
       ), $atts));

	$col_str = '' ;
	switch ($cols) {
		case '1' : $col_str = 'one' ; 			break;
		case '2' : $col_str = 'two' ; 			break;
		case '3' : $col_str = 'three' ; 		break;
		case '4' : $col_str = 'four' ; 			break;
		case '5' : $col_str = 'five' ; 			break;
		case '6' : $col_str = 'six' ; 			break;
		case '7' : $col_str = 'seven' ; 		break;
		case '8' : $col_str = 'eight' ; 		break;
		case '9' : $col_str = 'nine' ; 			break;
		case '10': $col_str = 'ten' ; 			break;
		case '11': $col_str = 'eleven' ; 		break;
		case '12': $col_str = 'twelve' ; 		break;
		case '13': $col_str = 'thirteen' ; 		break;
		case '14': $col_str = 'fourteen' ; 		break;
		case '15': $col_str = 'fifteen' ; 		break;
		case '16': $col_str = 'sixteen' ; 		break;
		case '17': $col_str = 'seventeen' ; 	break;
		case '18': $col_str = 'eighteen' ; 		break;
		case '19': $col_str = 'nineteen' ; 		break;
		case '20': $col_str = 'twenty' ; 		break;
		case '21': $col_str = 'twentyone' ; 	break;
		case '22': $col_str = 'twentytwo' ; 	break;
		case '23': $col_str = 'twentythree' ; 	break;
		case '24': $col_str = 'twentyfour' ; 	break;
		
		default:
     $col_str = 'twentyfour' ;
     break;
 }

 if($row != 'false' && $row != 0) echo "<div class='row' {$row}>";
 echo "<div class='$col_str columns' id='{$col_id}'>";
}
add_shortcode('zcols', 'tag_zurb_columns');

function tag_zurb_close($atts, $content = ''){
	echo "</div>";
}
add_shortcode('zc', 'tag_zurb_close');

/**
 * Tratamentos para adicionar as classes de menu ao menu principal do site
 *
 * @since Pacaembu 1.0
 */
add_filter('nav_menu_css_class','add_parent_css',10,2);
function  add_parent_css($classes, $item){
    global  $dd_depth, $dd_children;
    $classes[] = 'level-'.$dd_depth;
    if($dd_children) $classes[] = 'has-flyout';
    return $classes;
    }

class Pacaembu_Walker extends Walker_Nav_Menu {
    // function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
    //     $GLOBALS['dd_children'] = ( isset($children_elements[$element->ID]) )? 1:0;
    //     $GLOBALS['dd_depth'] = (int) $depth;
    //     parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    // }

    // function start_lvl(&$output, $depth) {
    //     $indent = str_repeat("\t", $depth);
    //     $output .= "\n$indent<ul class=\"flyout level-".$depth."\">\n";
    // }
    // 
    function display_element($element, &$children_elements, $max_depth, $depth=0, $args, &$output) {
        $element->has_children = !empty($children_elements[$element->ID]);
        // $element->classes[] = ($element->current || $element->current_item_ancestor) ? 'active' : '';
        $element->classes[] = ($element->has_children) ? 'has-flyout' : '';
		
        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }	
	
    function start_el(&$output, $item, $depth, $args) {
        $item_html = '';
        parent::start_el($item_html, $item, $depth, $args);	
		
        $classes = empty($item->classes) ? array() : (array) $item->classes;	
 
        if(in_array('has-flyout', $classes) && $depth == 0) {
            $item_html = str_replace('</a>', '</a><a class="flyout-toggle" href="#"><span> </span></a>', $item_html);
        }
		
        $output .= $item_html;
    }
 
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $output .= "\n<ul class=\"sub-menu flyout\">\n";
    }
}

/**
 * Tratamentos para adicionar as classes dos formulários
 *
 * @since Pacaembu 1.0
 */
add_filter( 'wpcf7_form_class_attr', 'pacaembu_cf7_classes');
function pacaembu_cf7_classes( $class ) {
	global $wpcf7;
	$class .= ' custom';
	return $class;
}

/**
 * Tratamentos para adicionar parametros de URL nos releases
 * @since Pacaembu 1.0
 */


add_action('generate_rewrite_rules', 'releases_datearchives_rewrite_rules');
function releases_datearchives_rewrite_rules($wp_rewrite) {
    $releases = releases_generate_date_archives('releases', $wp_rewrite);
    $wp_rewrite->rules = $releases + $wp_rewrite->rules;

    $eventos = releases_generate_date_archives('eventos', $wp_rewrite);
    $wp_rewrite->rules = $eventos + $wp_rewrite->rules;

    return $wp_rewrite;
}

function releases_generate_date_archives($cpt, $wp_rewrite) {
    $rules = array();
    $post_type = get_post_type_object($cpt);

    $slug_archive = $post_type->has_archive;
    if ($slug_archive === false) return $rules;
    if ($slug_archive === true) {
        $slug_archive = $post_type->name;
    }

    $dates = array(
        array(
            'rule' => "([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",
            'vars' => array('year', 'monthnum', 'day')),
        array(
          'rule' => "([0-9]{4})/([0-9]{1,2})",
          'vars' => array('year', 'monthnum')),
        array(
          'rule' => "([0-9]{4})",
          'vars' => array('year'))
    );

    foreach ($dates as $data) {
        $query = 'index.php?post_type='.$cpt;
        $rule = $slug_archive.'/'.$data['rule'];
        $i = 1;

        foreach ($data['vars'] as $var) {
            $query.= '&'.$var.'='.$wp_rewrite->preg_index($i);
            $i++;
        }
    }

    $rules[$rule."/?$"] = $query;
    $rules[$rule."/feed/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
    $rules[$rule."/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
    $rules[$rule."/page/([0-9]{1,})/?$"] = $query."&paged=".$wp_rewrite->preg_index($i);
    
    return $rules;
}


//removendo a barra admin
show_admin_bar(false);

include 'Mobile_Detect.php';
$detect = new Mobile_Detect();

