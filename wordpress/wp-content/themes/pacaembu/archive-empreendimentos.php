<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();

$args = array(
    'post_type' => 'empreendimentos',
    'posts_per_page' => -1,
    'order' => 'ASC',
    'orderby' => 'title'
);
query_posts( $args );
?>
<div class="conteudo">
    <div class="row">
        <div class="twelve columns">
            <h3 class="titulo preto">Obras em Andamento</h3>
        </div>
        <div class="twelve columns">
            <ul class="breadcrumbs">
                <li><a href="#mapa">Mapa</a></li>
            </ul>
        </div>
    </div>
    <div class="row listagem-items">
        <div class="twentyfour columns">
        <?php if ( have_posts() ) : ?>
            <ul class="block-grid two-up">
            <?php while ( have_posts() ) : the_post();
                if ( ! get_field('somente_lista_empreendimento') ) {
                    if ( ! get_field("obra_em_andamento_fixo") ) {
                        $total_evolucao = 0;
                        $evolucoes = get_field('evolucao');
                        foreach ($evolucoes as $mod) {
                            $total_evolucao += $mod['percentual'];
                        }

                        $media_evolucao = round( $total_evolucao / count( $evolucoes ) );
                        if( $media_evolucao > 99 ) continue;
                    }

                    $logo = get_field('logo');
                    ?>
                    <li class="media">
                        <div class="img linha-sobre">
                            <a href="<?php echo get_permalink($post->ID); ?>" class="container-img"><img src="<?php echo $logo['url'] ?>" /></a>
                        </div>
                        <div class="body">
                            <h4><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>
                            <p><strong><?php the_field("cidade"); ?></strong><br>
                                <?php the_field("dorms"); ?><br>
                                <?php the_field("area"); ?></p>
                        </div>
                    </li>
                <?php
                }
            endwhile;
            ?></ul>
        <?php else :
            get_template_part( 'content', 'none' );
        endif;
        ?>
        </div>
    </div>
    <div class="row subarea" id="mapa">
        <div class="twelve columns">
            <h3 class="titulo vermelho">Mapa</h3>
        </div>
        <div class="twelve columns">
            <p class="chamada">Confira no mapa todos os nossos empreendimentos.</p>
        </div>
    </div>

    <div class="row mapa">
        <div class="container-mapa">
            <div id="map-canvas"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>