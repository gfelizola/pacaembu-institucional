<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Pacaembu consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();
?>

<?php
$destaques = new WP_query( array(
	'post_type' => 'destaques',
	'post_per_page' => -1
));

if( $destaques->have_posts() ):
?>
<div class="destaques">
    <div class="container-destaques">
    <?php
    $c = 0;
    while( $destaques->have_posts() ):
        $c++;
    	$destaques->the_post();
    	$link_endereco = get_field('link');
    	$imagem = get_field('imagem');
    	?>
        <div data-caption="#legenda<?php echo $c?>">
            <a href="<?php echo get_permalink($link_endereco) ?>"><img src="<?php echo $imagem['url'] ?>" alt="" /></a>
        </div>
    <?php endwhile; ?>
    </div>
    <?php
    $c = 0;
    while( $destaques->have_posts() ):
        $destaques->the_post();
        $c++;
    ?>
    <a href="<?php echo get_permalink($link_endereco) ?>" class="orbit-caption" id="legenda<?php echo $c ?>"><?php the_title() ?></a>
    <?php endwhile; ?>
</div>
<?php endif; ?>

<div class="botoes-atalhos">
    <div class="row">
        <a href="<?php echo site_url() ?>/nossos-empreendimentos" class="botao mobile-four"><span data-icon="&#xe00c;"></span>Empreendimentos</a>
        <a href="<?php echo site_url() ?>/lancamentos" class="botao mobile-four"><span data-icon="&#xe00b;"></span> Lançamentos</a>
        <a href="<?php echo site_url() ?>//venda-seu-terreno" class="botao mobile-four"><span data-icon="&#xe009;"></span>Venda sua área</a>
        <a href="<?php echo site_url() ?>/releases" class="botao mobile-four"><span data-icon="&#xe00a;"></span> Notícias</a>
    </div>
</div>

<div class="row">
    <div class="eight columns chamada-destaque">
        <h3 class="titulo vermelho">Obra Online</h3>
        <?php
        $camera_flag = get_field('home_obraonline_camera_flag','options');

        if( $camera_flag == 'Câmera' ){
            $url_camera = get_field('url_camera_obraonline','options');
            if ($detect->isMobile()) {
                $url_camera = get_field('url_camera_obraonline_mobile','options');
            }

            echo '<iframe src="' . $url_camera . '" width="300" height="240" allowTransparency="true" frameborder="0" scrolling="no"></iframe>';
        } else {
            $imagem = get_field('imagem_obraonline','options');
        ?>
        <a href="<?php bloginfo('site_url') ?>/empreendimento/" class="link-lupa linha-sobre">
            <img src="<?php echo $imagem['url'] ?>" alt="">
            <span class="lupa">ver mais</span>
        </a>
        <?php } ?>
        <p><?php the_field('descricao_obraonline','options') ?></p>
    </div>

    <div class="sixteen columns obras">
        <h3 class="titulo vermelho"><a href="<?php echo site_url() ?>/empreendimentos">Obras em andamento</a></h3>
        <?php
        $obras = new WP_Query(array(
            'posts_per_page' => -1,
            'post_type'      => 'empreendimentos',
            'meta_key'       => 'evolucao_home',
            'meta_value'     => 1,
            'orderby'        => 'title',
            'order'          => 'ASC'
        ));

        if( $obras->have_posts() ){
            echo '<ul class="block-grid two-up">';
            while ( $obras->have_posts() ) {
                $obras->the_post();
                $evolucoes = get_field('evolucao');

                $total_evolucao = 0;
                foreach ($evolucoes as $mod) {
                    $total_evolucao += $mod['percentual'];
                }

                $media_evolucao = round( $total_evolucao / count( $evolucoes ) );

                ?>
                <li>
                    <div class="porcentagem">
                        <div class="valor p<?php echo $media_evolucao ?>">
                            <div><?php echo $media_evolucao ?>%</div>
                        </div>
                    </div>
                    <span><a href="<?php echo get_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></span>
                </li>
                <?php
            }
            echo '</ul>';
        }
        ?>
    </div>
</div>

<?php get_footer(); ?>