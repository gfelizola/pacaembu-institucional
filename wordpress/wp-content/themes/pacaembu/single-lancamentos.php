<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

<div class="lancamentos-detalhe">
    <div class="row">
        <div class="twentyfour columns">
            <?php the_breadcrumb(); ?>
        </div>
    </div>

    <div class="row destaques">
        <div class="twentyfour columns">
            <div class="container-destaques">
                <?php $destaque = get_field('destaque'); ?>
                <img src="<?php echo $destaque['url']; ?>" alt="">
                <div class="descritivo">
                    <h3 class="titulo branco">Descritivo</h3>
                    <?php the_field('descritivo') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row subarea">
        <div class="sixteen columns">
            <h3 class="titulo vermelho">Ficha Técnica</h3>
            <div class="panel"><?php the_field('ficha_tecnica') ?></div>
        </div>
        <div class="eight columns banners">
            <div class="row">
                <div class="twentyfour columns telefones" style="background-color: <?php the_field('inscricao_cor_bg'); ?>; color: <?php the_field('inscricao_cor_txt'); ?>">
                    <p>
                        <span class="icone-telefone"></span><br>
                        <span class="inscricoes">informações</span>
                    </p>

                    <p><?php
                    $telefones = get_field('telefones_inscricao');
                    foreach ($telefones as $tel) {
                        echo  $tel['telefone'] . "<br>";
                    }
                    ?></p>

                </div>
            </div>
            <?php
            $banners = get_field('patrocinadores');
            foreach ($banners as $banner) {
                ?>
                <p><img src="<?php echo $banner['imagem_banner']['url'] ?>" /></p>
                <?php
            }
            ?>
        </div>
    </div>

    <?php 
    $galeria = get_field('plantas');
    if( $galeria ){
    ?>
    <div class="row linha-sobre">
        <div class="twentyfour columns">
            <h3 class="titulo vermelho">Plantas</h3>
            <div class="lista-galeria">
                <ul>
                    <?php
                        foreach ($galeria as $img) {
                            $data_atualizacao = get_post_modified_time( 'j F Y', false, $img['id'], true );
                            echo "<li><a rel='plantas-group' title='" . $img['caption'] . "' class='fancybox' href='" . $img['url'] . "'><img src='" . $img['sizes']['thumbnail'] . "' /></a></li>";
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <?php 
    }
    ?>
    

    <div class="row subarea linha-sobre">
        <div class="twentyfour columns">
            <h3 class="titulo vermelho">Localização</h3>
            <p><strong class="vermelho"><?php the_field('cidade') ?></strong><br>
                Endereço: <strong><?php the_field('endereco') ?></strong><br></p>
            <div class="mapa">
                <?php $local = get_field('localizacao_lancamento'); ?>
                <div class="container-mapa">
                    <div id="map-canvas" data-localizacao="<?php echo $local['coordinates'] ?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>