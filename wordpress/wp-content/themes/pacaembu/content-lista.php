<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>
	<li>
		<h4 class=""><a href="<?php echo get_permalink()?>"><?php the_title(); ?></a></h4>
		<?php the_excerpt(); ?>
	</li>