<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="twentyfour columns">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page-date' ); ?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>