<?php
/**
 * Template Name: Template de página com barra lateral
 *
 * Description: Use este template para remover a barra lateral de qualquer página
 *
 * Dica: para remover a barra lateral de todos os posts e páginas, basta remover todos os widgets da barra lateral.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="sixteen columns">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile;?>
			</div>
			<div class="eight columns">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>