<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<div class="entry-header twentyfour colums">
				<?php the_breadcrumb(); ?>
				<h3 class="entry-title titulo preto"><?php the_title(); ?></h3>
			</div>
		</div>

		<div class="row">
			<div class="entry-content twentyfour columns">
				<p><?php the_date() ?></p>
				<?php the_content(); ?>
			</div>
		</div>
	</div><!-- #post -->
