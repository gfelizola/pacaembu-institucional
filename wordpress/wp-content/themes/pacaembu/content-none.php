<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>

	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			<h1 class="entry-title">Nada encontrado</h1>
            <p><?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $pt = (get_query_var('post_type')) ? get_query_var('post_type') : 1;
            echo "$pt - $paged";
            ?></p>
		</header>

		<div class="entry-content">
			<p>Não foi possível encontrar o conteúdo que estava procurando. Por favor, tente fazer uma busca no site.</p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
