<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="twentyfour columns">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page-date' ); ?>
					<?php
					$galeria = get_field('galeria');

					if($galeria):?>

					

					<div class="container">
						<div class="row" id="name">
							<div class="lista-galeria">
				                <ul>
				                    <?php
				                        foreach ($galeria as $img) {
				                            $data_atualizacao = get_post_modified_time( 'j F Y', false, $img['id'], true );
				                            echo "<li><a rel='eventos-group' title='" . $img['caption'] . "' class='fancybox' href='" . $img['url'] . "'><img src='" . $img['sizes']['thumbnail'] . "' /></a></li>";
				                        }
				                    ?>
				                </ul>
				            </div>
						</div>
					</div>
					<?php endif;?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>