<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

<div class="conteudo">
	<div class="row">
		<div class="twentyfour columns">
			<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', 'page' ); ?>
		<?php endwhile;?>

		<?php
		$lista_completa = array();

		$q_empreendimentos = new WP_Query(array(
			'post_type' => 'empreendimentos', 
			'posts_per_page' => '-1', 
			'order' => 'ASC', 
			'orderby' => 'title'
		));
		while($q_empreendimentos->have_posts()){
			$q_empreendimentos->the_post();

			if( get_field('listar_empreendimento') ) continue;

			$total_evolucao = 0;
			$status = '';
			$evolucoes = get_field('evolucao');
			if ($evolucoes) {
				foreach ($evolucoes as $mod) {
					$total_evolucao += $mod['percentual'];
				}

				$media_evolucao = round( $total_evolucao / count( $evolucoes ) );
				$status = $media_evolucao > 99 ? 'Entregue' : 'Em Obras' ;
			}

			$local = get_field('localizacao');
			$logo = get_field('logo');
			$descricao = "<p><strong>" . get_field("cidade") . "</strong><br>" . get_field("dorms") ." <br>" . get_field("area") . "</p>";

			$info = array(
				'nome'        => get_the_title(),
				'cidade'      => get_field('cidade'), 
				'unidades'    => get_field('unidades'), 
				'status'      => $status,
				'link'        => get_field('somente_lista_empreendimento') ? '' : get_permalink(),
				'localizacao' => "{ nome: '" . get_the_title() . "', local: new google.maps.LatLng(" . $local['coordinates'] . "), logo: '" . $logo['url'] . "', descricao: '$descricao' },"
			);

			$lista_completa[] = $info;
		}

		$q_lancamentos = new WP_Query(array(
			'post_type' => 'lancamentos', 
			'posts_per_page' => '-1', 
			'order' => 'ASC', 
			'orderby' => 'title'
		));
		while($q_lancamentos->have_posts()){
			$q_lancamentos->the_post();
			$local = get_field('localizacao_lancamento');
			$logo = get_field('logo');
			$descricao = "<p><strong>" . get_field("cidade") . "</strong><br>" . get_field("dorms") ." <br>" . get_field("area") . "</p>";

			$info = array(
				'nome'        => get_the_title(),
				'cidade'      => get_field('cidade'), 
				'unidades'    => '-', 
				'status'      => 'Lançamento',
				'link'        => get_permalink(),
				'localizacao' => "{ nome: '" . get_the_title() . "', local: new google.maps.LatLng(" . $local['coordinates'] . "), logo: '" . $logo['url'] . "', descricao: '$descricao' },"
			);

			$lista_completa[] = $info;
		}

		function ordena_lista($a,$b){
			return strcasecmp($a['nome'],$b['nome']);
		}

		usort($lista_completa, 'ordena_lista');
		?>
	</div>
</div>
<div class="row">
	<table class="twentyfour">
		<thead>
			<tr>
				<th>Empreendimento</th>
				<th>Cidade</th>
				<th>Status</th>
				<th>Unidades</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($lista_completa as $emp) {
				echo '<tr><td>';
				if($emp['link'] != '') echo '<a href="' . $emp['link'] . '" class="vermelho">';
				echo '<strong>' . $emp['nome'] . '</strong>';
				if($emp['link'] != '') '</a>';
				echo '</td>
					<td>' . $emp['cidade'] . '</td>
					<td>' . $emp['status'] . '</td>
					<td>' . $emp['unidades'] . '</td>
				</tr>';
			}
			?>
		</tbody>
	</table>
</div>


<div class="row subarea linha-sobre">
	<div class="twentyfour columns">
		<h3 class="titulo vermelho">Localização</h3>
		<div class="mapa">
			<div class="container-mapa">
				<div id="map-canvas"></div>
			</div>
		</div>
	</div>
</div>

<script>
if(posicoesEmpreendimentos == undefined){
	var posicoesEmpreendimentos = [
		<?php
		foreach ($lista_completa as $emp) {
			echo $emp['localizacao'];
		}
		?>
	];

	console.log( posicoesEmpreendimentos );
}
</script>

<?php get_footer(); ?>