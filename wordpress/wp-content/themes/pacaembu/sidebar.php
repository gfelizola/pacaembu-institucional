<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<div class="noticias-twitter">
                <h3><a href="http://twitter.com/pacaembuconstru">Notícias</a></h3>
                <ul>
                <?php include 'noticias.php'; ?>
                </ul>
            </div>

            <p><a title="Nossos Empreendimentos" href="<?php echo site_url() ?>/nossos-empreendimentos/"><img class="alignnone size-full wp-image-114" alt="chamada-nossos-empreendimentos" src="<?php echo bloginfo('template_url') ?>/images/chamada-nossos-empreendimentos.jpg" width="275" height="143" /></a></p>
            <p><a title="Venda seu terreno" href="<?php echo site_url() ?>/venda-seu-terreno/"><img class="alignnone size-full wp-image-115" alt="Venda seu terreno" src="<?php bloginfo('template_url') ?>/images/chamada-venda-seu-terreno.jpg" width="268" height="99" /></a></p>
			
			
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>