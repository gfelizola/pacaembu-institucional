<?php
/**
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="twentyfour columns">
				<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>
				<?php endwhile;?>
				<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; // end have_posts() check ?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>