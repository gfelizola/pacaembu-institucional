<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="sixteen columns listagem-items">

			<?php if ( have_posts() ) : ?>
				<h3 class="page-title titulo preto"><?php printf( __( 'Resultados de busca para: %s', 'pacaembu' ), '<span class="vermelho"><strong>' . get_search_query() . '</strong></span>' ); ?></h1>
				<ul>
					<?php while ( have_posts() ) : the_post(); ?>
					<li class="media">
						<?php 
							$img = get_field('logo');
							if( $img ){
								echo "<div class='img'><a href='" . get_permalink() . "' class='container-img'><img src='" . $img['url'] . "' /></a></div>" ;
							} else {
								the_post_thumbnail(); 
							}
						?>
						<div class="body">
							<h3 class="entry-title">
								<a href="<?php the_permalink(); ?>" title="<?php echo the_title_attribute( 'echo=0' ); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h3>
							<?php pacaembu_entry_meta(); ?>
						</div>
					</li>
					<?php endwhile; ?>
				</ul>
			<?php else : ?>

				<div id="post-0" class="post no-results not-found">
					<h3 class="entry-title"><?php _e( 'Nada encontrado.', 'pacaembu' ); ?></h3>

					<div class="entry-content">
						<p><?php _e( 'Nenhum resultado encontrado para as palavras buscas. Tente trocar as palavras e realizar uma nova busca.', 'pacaembu' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->

			<?php endif; ?>

			</div><!-- #columns -->
			<div class="eight columns">
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #row -->
	</div><!-- #content -->
<?php get_footer(); ?>