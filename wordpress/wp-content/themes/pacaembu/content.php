<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="row">
			<div class="entry-header twentyfour colums">
				<?php the_breadcrumb(); ?>
				<h3 class="entry-title titulo preto"><?php the_title(); ?></h3>
			</div>
		</div>

		<div class="row">
			<div class="entry-content twentyfour columns">
				<?php the_content(); ?>
				<?php // wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
				<?php edit_post_link( __( 'Editar', 'pacaembu' ), '<span class="edit-link">', '</span>' ); ?>
			</div>
		</div>
	</div><!-- #post -->
