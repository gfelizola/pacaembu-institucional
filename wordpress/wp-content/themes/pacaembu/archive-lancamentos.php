<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();
query_posts( 'post_type=lancamentos&posts_per_page=-1&order=ASC&orderby=title' );
?>
<div class="conteudo">
	<div class="row">
        <div class="twentyfour columns">
            <h3 class="titulo preto">Lançamentos</h3>
        </div>
    </div>
    <div class="row listagem-items">
    	<div class="twentyfour columns">
		<?php if ( have_posts() ) : ?>
			<ul class="block-grid two-up">
			<?php while ( have_posts() ) : the_post();
				$logo = get_field('logo');
				?>
				<li class="media">
	                <div class="img linha-sobre">
	                    <a href="<?php echo get_permalink($post->ID); ?>" class="container-img"><img src="<?php echo $logo['url'] ?>" /></a>
	                </div>
	                <div class="body">
	                    <h4><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>
	                    <p><strong><?php the_field("cidade"); ?></strong><br>
	                        <?php the_field("dorms"); ?><br>
	                        <?php the_field("area"); ?></p>
	                </div>
	            </li>
				<?php
			endwhile;
			?></ul>
		<?php else :
			get_template_part( 'content', 'none' );
		endif;
		?>
    	</div>
	</div>
    <div class="row subarea linha-sobre" id="mapa">
        <div class="twelve columns">
            <h3 class="titulo vermelho">Mapa</h3>
        </div>
        <div class="twelve columns">
            <p class="chamada">Confira no mapa todos os nossos lançamentos.</p>
        </div>
    </div>

    <div class="row mapa subarea">
        <div class="container-mapa">
            <div id="map-canvas"></div>
        </div>
    </div>
</div>
<script>
if(posicoesEmpreendimentos == undefined){
    var posicoesEmpreendimentos = [
    <?php
    $empreendimentos = new WP_Query("post_type=lancamentos&post_per_page=-1");
    if( $empreendimentos->have_posts() ){
        while ($empreendimentos->have_posts()) {
            $empreendimentos->the_post();
            $local = get_field('localizacao_lancamento');
            // echo "new google.maps.LatLng(" . $local['coordinates'] . ")," ;

            $logo = get_field('logo');
            $descricao = "<p><strong>" . get_field("cidade") . "</strong><br>" . get_field("dorms") ." <br>" . get_field("area") . "</p>";
            echo "{ nome: '" . get_the_title() . "', local: new google.maps.LatLng(" . $local['coordinates'] . "), logo: '" . $logo['url'] . "', descricao: '$descricao' },";
        }
    }
    ?>
    ];
}
</script>
<?php get_footer(); ?>