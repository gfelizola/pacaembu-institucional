<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();

global $query_string;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts( $query_string . '&posts_per_page=5&paged=' . $paged );

?>
<div class="conteudo">
    <div class="row">
        <div class="sixteen columns">
            <div class="row">
                <div class="entry-header twentyfour colums">
                    <?php the_breadcrumb(); ?>
                    <h3 class="entry-title titulo preto">Publicações</h3>
                </div>
            </div>
            <div class="row">
                <ul class="block-grid three-up mobile-two-up">
                <?php while ( have_posts() ) : the_post(); ?>
                    <li class="item-publicacao">
                        <?php
                            $edicao  = get_field('edicao');
                            $capa    = get_field('capa');
                            $arquivo = get_field('arquivo_publicacao');

                            echo "<a href=\"$arquivo\" class=\"th link-lupa\" target=\"_blank\">
                                <img src=\"" . $capa['sizes']['medium'] . "\"  />
                                <h3>" . get_the_title() . "
                                    <small>Ed. $edicao</small></h3>
                                <span class=\"lupa\">baixar</span>
                            </a>";
                        ?>
                    </li>
                <?php endwhile;?>
                </ul>
                <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
            </div>
        </div>
        <div class="eight columns">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>