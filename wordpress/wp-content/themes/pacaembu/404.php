<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header(); ?>

	<div class="conteudo">
		<div class="row">
			<div class="twentyfour columns">
				<?php get_template_part( 'content', 'none' ); ?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>