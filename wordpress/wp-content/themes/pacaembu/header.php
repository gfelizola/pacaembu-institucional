<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */
?><!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php
        // if (is_archive()) post_type_archive_title();
        wp_title( ' | ', true, 'right' );
    ?></title>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascripts/modernizr.foundation.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx8p9HH99lCwJCIcI1GwHVRZt-el0Ze1Y&sensor=false"></script>
	<?php wp_head(); ?>
</head>

<?php
$grande='';
$post_type = get_post_type();
$post_types_grandes = array('empreendimentos','lancamentos');

if( is_front_page() || (in_array($post_type, $post_types_grandes) && ! is_archive() ) ){
    $grande = 'bg-grande';
}
?>
<body <?php body_class($grande); ?>>
    <?php
    // $menus = get_registered_menus();
     ?>

     <div class="barra-preta">
        <div class="row">
            <div class="twentyfour columns menu-secundario">
                <dl class="sub-nav">
                    <dd><a href="<?php the_field('link_webmail','options') ?>" class="webmail" target="_blank"><span data-icon="&#xe000;"></span> WEBMAIL</a></dd>
                    <dd><a href="<?php the_field('link_intranet','options') ?>" class="intranet" target="_blank"><span data-icon="&#xe001;"></span> INTRANET</a></dd>
                    <dd><a href="<?php echo site_url() ?>/trabalhe-conosco/" class="trabalhe-conosco"><span data-icon="&#xe002;"></span> TRABALHE CONOSCO</a></dd>
                    <dd><a href="<?php echo site_url() ?>/venda-seu-terreno" class="venda-seu-terreno"><span data-icon="&#xe007;"></span> VENDA SUA ÁREA</a></dd>
                    <dd><a href="<?php echo site_url() ?>/ouvidoria" class="trabalhe-conosco"><span data-icon="&#xe005;"></span> FALE COM A OUVIDORIA</a></dd>
                </dl>
            </div>
        </div>
     </div>

	<div class="row header">
        <div class="eight columns">
            <h1 class="logo"><a href="<?php echo site_url() ?>">Grupo Pacaembu Construtora - Haus Construtora</a></h1>
        </div>
        <?php wp_nav_menu( array(
            'theme_location'  => 'Principal',
            'container_class' => 'sixteen columns menu-principal',
            'menu_class'      => 'menu nav-bar',
            // 'depth'           => 3,
            'walker'          => new Pacaembu_Walker
        )); ?>

    </div>