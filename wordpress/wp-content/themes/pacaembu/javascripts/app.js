;(function ($, window, undefined) {
    'use strict';

    var $doc = $(document),
    Modernizr = window.Modernizr;

    $(document).ready(function() {
        $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
        $.fn.foundationButtons          ? $doc.foundationButtons() : null;
        $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
        $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
        $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
        $.fn.foundationCustomForms      ? $doc.foundationCustomForms({prefixContent:'<img src="' + url_template + '/images/sprites/seta-vermelha-arquivo.png" />'}) : null;
        $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
        $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
        $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
        $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
        $.fn.foundationClearing         ? $doc.foundationClearing() : null;

        $.fn.placeholder                ? $('input, textarea').placeholder() : null;
    });

    $('.block-grid.two-up>li:nth-child(2n+1)').css({clear: 'both'});
    $('.block-grid.three-up>li:nth-child(3n+1)').css({clear: 'both'});
    $('.block-grid.four-up>li:nth-child(4n+1)').css({clear: 'both'});
    $('.block-grid.five-up>li:nth-child(5n+1)').css({clear: 'both'});

    // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
    if (Modernizr.touch && !window.location.hash) {
        $(window).load(function () {
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        });
    }
})(jQuery, this);

function inicializaMapa() {
    var marker;
    var map;
    var SaoPaulo = new google.maps.LatLng(-22.177232,-47.845459);
    var local = $('#map-canvas').data('localizacao');

    if( local ){
        var localArr = local.split(",");
        var centro = new google.maps.LatLng(localArr[0],localArr[1]);
        var zoomInicial = 14 ;
        posicoesEmpreendimentos = [ {local:centro} ];
    } else {
        var centro = SaoPaulo;
        var zoomInicial = 7 ;
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: zoomInicial,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centro
    });

    var markers = [];
    var infowindows = [];
    var infowindow = null;

    for (var i = posicoesEmpreendimentos.length - 1; i >= 0; i--) {
        var p = posicoesEmpreendimentos[i];

        markers[i] = new google.maps.Marker({
            map:map,
            position: p.local,
            icon: url_template + '/images/sprites/casinha-logo.png',
            infoWindowIndex: i
        });

        if( p.nome !== undefined ){
            var conteudo = '<div class="infowindow-conteudo">' +
                '<img src="' + p.logo + '" />' +
                '<h3>' + p.nome + '</h3>' +
                '<div class="info-corpo">' +
                    
                    p.descricao +
                '</div>';

            // infowindows[i] = new google.maps.InfoWindow({
            //     content: conteudo
            // });
            
            var texto = $('<div />');
            texto.addClass('box-info');
            texto.html(conteudo);

            infowindows[i] = new InfoBox({
                content: texto.get(0)
                ,position: p.local
                ,alignBottom: true
                ,disableAutoPan: false
                ,pixelOffset: new google.maps.Size(50, 100)
                ,boxStyle: { width: "220px" }
                ,closeBoxMargin: "-13px -20px 0px 0px"
                ,closeBoxURL: url_template + "/stylesheets/fancybox/fancybox_close.png"
                ,infoBoxClearance: new google.maps.Size(10, 10)
            });

            google.maps.event.addListener(markers[i], 'click', function(e) {
                if( infowindow ) infowindow.close();
                infowindow = infowindows[this.infoWindowIndex]
                infowindow.open(map, this);
            });
        }
        
    };
}

$(window).load(function() {
    if( $('#map-canvas').length ) inicializaMapa();
    if( $('a.camera-player').length ) inicializarPlayer();

     $('.home .container-destaques').orbit({
        fluid: '16x6',
        animation: 'horizontal-push',
        animationSpeed: 1500,
        advanceSpeed: 5000,
        captions: true,
        captionAnimation: 'fade',
        bullets: true
    });
});

$(document).ready(function() {
    // Menu
    $menuItem = $("ul.menu > li");
    $menuItem.on("mouseenter mouseleave", function(ev){
        var animaSpeed = 300;
        var $self = $(this);
        var $link = $self.find('> a:eq(0)');
        var $submenu = $link.next();
        if(ev.type == "mouseenter")
        {
            $link.animate({backgroundColor: "#d21000"}, animaSpeed);
            $self.animate({backgroundColor: "#d21000"}, animaSpeed);
            $submenu.slideDown({queue: false, duration: animaSpeed});
        }
        else
        {
            $submenu.slideUp({queue: false, duration: animaSpeed});
            animateMenu($link, $self, 100);
        }
    });

    function animateMenu(link, self, animaSpeed)
    {
        // IE
        if(jQuery.support.opacity)
        {
            link.animate({backgroundColor: "transparent"}, animaSpeed);
            self.animate({backgroundColor: "transparent"}, animaSpeed);
        }
        else{
            link.removeAttr('style');
            self.removeAttr('style');
        }
    }

    $(".fancybox:not([rel='camera-group'][rel='plantas-group'])").fancybox({
        padding: 5,
        beforeShow: function () {
            if (this.title) {
                this.title += '<br />';
            } else {
                this.title = '';
            }
            this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="' + this.href + '">Tweet</a> ';
            this.title += '<iframe src="//www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
        },
        afterShow: function() {
            twttr.widgets.load();
        },
        helpers : {
            thumbs  : {
                width  : 80,
                height : 80
            },
            title : {
                type: 'inside'
            }
        },

    });

    $(".fancybox[rel='camera-group']").fancybox({
        padding: 0,
        margin: 0,
        width: 710,
        height: 540,
        autoScale: false
    });

    $(".fancybox[rel='plantas-group']").fancybox({
        padding: 0,
        margin: 10,
        title : {
            type: 'inside'
        }
    });

    $('.telefone-mascara').mask("(99) 9999-9999?9");
    $('.cep-mascara').mask("99999-999");

    $('.lista-galeria').jcarousel();

    $('div.footer').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
        if (isInView) {
            $('div.barra-contato').stop().animate({bottom:($('div.barra-contato').height()+30)*-1});
        } else {
            $('div.barra-contato').stop().animate({bottom:0});
        }
    });

    var sliderUnidades = $('div.barra-contato .slider-unidades');

    sliderUnidades.find('.container-slides ul li:first').addClass('atual')
    sliderUnidades.find('.seta').click(function(e) {
        e = e || event;
        if(e) e.preventDefault();

        var slides = sliderUnidades.find('.container-slides ul li');
        var atual = slides.filter('.atual').index();

        slides.removeClass('atual');


        console.log(slides, atual);

        if( $(this).hasClass('direita') ){
            atual++;
            if( atual >= slides.length ) atual = 0;
        } else {
            atual--;
            if( atual < 0 ) atual = slides.length -1;
        }

        slides.eq(atual).addClass('atual');

        sliderUnidades.find('.container-slides ul').stop().animate({left: ( atual * 360 ) * -1});

    });
});