<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();

$ano_var = (get_query_var('year')) ? get_query_var('year') : date('Y');

global $query_string;
query_posts( $query_string . '&year=' . $ano_var );

$todos_releases = new WP_Query('post_type=releases&posts_per_page=-1');
$anos = array();

while ($todos_releases->have_posts()) {
    $todos_releases->the_post();
    $ano_release = get_the_time('Y');
    if( ! in_array( $ano_release, $anos ) ){
        $anos[] = $ano_release;
    }
}

?>
<div class="conteudo">
    <div class="row">
        <div class="entry-header twentyfour columns">
            <?php the_breadcrumb(); ?>
        </div>
    </div>
    <div class="row">
        <div class="twentyfour columns">
            <dl class="tabs">
                <?php
                foreach ($anos as $ano) {
                    $classe = '';
                    if( $ano == $ano_var ) $classe = " class='active'";
                    echo "<dd$classe><a href='" . site_url() . "/releases/$ano'>$ano</a></dd>";
                }
                ?>
            </dl>
        </div>
    </div>
    <div class="row">
    	<div class="twenty columns">
		<?php if ( have_posts() ){?>
			<ul>
			<?php
			while ( have_posts() ) : the_post();
				?><li>
                    <h4 class=""><a href="<?php echo get_permalink()?>"><?php echo get_the_date('d/m/Y'); ?> - <?php the_title(); ?></a></h4>
                    <?php the_excerpt(); ?>
                </li><?php
			endwhile;
			?></ul>
            <?php 
            if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
		} else {
			get_template_part( 'content', 'none' );
		}
		?>
    	</div>

    	<div class="four columns">
    		<?php wp_get_archives( array( 
				'type'            => 'yearly', 
				'format'          => 'html', 
				'show_post_count' => 1,
				'echo' => 1
			)); ?>
    	</div>
	</div>
</div>
<?php get_footer(); ?>