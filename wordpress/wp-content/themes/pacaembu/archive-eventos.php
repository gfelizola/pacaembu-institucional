<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Pacaembu already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Pacaembu
 * @since Pacaembu 1.0
 */

get_header();

global $query_string;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts( $query_string . '&posts_per_page=5&paged=' . $paged );

?>
<div class="conteudo">
    <!-- <div class="row">
        <div class="entry-header twentyfour colums">
            <?php the_breadcrumb(); ?>
        </div>
    </div> -->
    <div class="row">
        <div class="twenty columns">
            <?php if ( have_posts() ){?>
            <ul>
            <?php 
            $gal_i = 0;
            while ( have_posts() ) : the_post(); 
            $gal_i++;
            ?>
                <li>
                    <h4 class=""><?php the_title(); ?></h4>
                    <?php the_content(); ?>
                    <?php
                    $galeria = get_field('galeria');
                    if($galeria):?>
                    <div class="container">
                        <div class="row">
                            <div class="lista-galeria">
                                <ul>
                                    <?php
                                    
                                    foreach ($galeria as $img) {
                                        
                                        $data_atualizacao = get_post_modified_time( 'j F Y', false, $img['id'], true );
                                        echo "<li><a rel='eventos-group-$gal_i' title='" . $img['caption'] . "' class='fancybox' href='" . $img['url'] . "'><img src='" . $img['sizes']['thumbnail'] . "' /></a></li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                </li>
            <?php endwhile;?>
            </ul>
            <?php 
            if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
            } else {
                get_template_part( 'content', 'none' );
            }
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>