<?php
// Define Environments
$environments = array(
    'local'    => 'localhost',
    'local2'   => '192.168',
    'producao' => 'www.pacaembuconstrutora.com.br'
);

$server_name = $_SERVER['SERVER_NAME'];
foreach($environments AS $key => $env){
    if(strstr($server_name, $env)){
        define('ENVIRONMENT', $key);
        break;
    }
}

if(!defined('ENVIRONMENT')) define('ENVIRONMENT', 'production');

switch(ENVIRONMENT){
    case 'local':
        define('DB_NAME', 'pacaembu');
        define('DB_USER', 'root');
        define('DB_PASSWORD', 'gustavof');
        define('DB_HOST', 'localhost');
        define('WP_SITEURL', 'http://localhost:8080/pacaembu/wordpress/');
        define('WP_HOME', 'http://localhost:8080/pacaembu/wordpress/');
        define('WP_DEBUG', true);
        break;

    case 'local2':
    case 'producao':
        define('DB_NAME', 'pacaembuconstr09');
        define('DB_USER', 'pacaembuconstr09');
        define('DB_PASSWORD', 'pacaembu2013');
        define('DB_HOST', 'mysql.pacaembuconstrutora.com.br');
        define('DB_CHARSET', 'utf8');
        define('DB_COLLATE', '');
        define('WP_DEBUG', false);
        break;
}

if(!defined('DB_NAME'))     define('DB_NAME', 'pacaembuconstr09');
if(!defined('DB_USER'))     define('DB_USER', 'pacaembuconstr09');
if(!defined('DB_PASSWORD')) define('DB_PASSWORD', 'pacaembu2013');
if(!defined('DB_HOST'))     define('DB_HOST', 'mysql.pacaembuconstrutora.com.br');
if(!defined('DB_CHARSET'))  define('DB_CHARSET', 'utf8');
if(!defined('DB_COLLATE'))  define('DB_COLLATE', '');
if(!defined('WP_DEBUG'))    define('WP_DEBUG', false);

define('AUTH_KEY',         '.x6FJvMhn$)3_1`]j_K.S_%+7q#qaT{#uSeZ#4Kffb|*jWhXN Y1AW`Mfon)/i$i');
define('SECURE_AUTH_KEY',  'd(`<S`X-U~;_EgP*fh-V9]6?@O~}iDyD`-U(IKi,Bd[_5=}1]{un2nsU9evlQqR|');
define('LOGGED_IN_KEY',    '*svqGNmHgx&*cUXvl[!ZF%jh]gw7+qSNNI[mQs)tb;l$J(UEiVBd NIJL ,+<Smf');
define('NONCE_KEY',        'p{N%6Xo@cE/ b2PLfepmNM9V~]TTE@z|r-w4q=2VU%wYd/n3xc.H^DsbZ$:sV`Eu');
define('AUTH_SALT',        'RHkaWOh#e%NBMz;x7@M<aO`&r@e:%ws%gpMr/W-8n$+[$)uyZDP+z1as2B==W`{*');
define('SECURE_AUTH_SALT', 'L>@gjxKK3o>()tC,#>DJG1LRuD+OQB`B)01-ZS,^E[(Xlh.{,C{B8[/^H+HBxR s');
define('LOGGED_IN_SALT',   'ovaaU=#3gF,:eX=/~KR*6%%Kfuo>A)X8aV{GJ6YSQnFI[=r|h2{6kv!w mMTa*UR');
define('NONCE_SALT',       'hCOxe[W)<0Bn2=P(*Jw3G!NcSn8;&MCv%q`#C5lYdB8<IGt^5vBi|IsXTph f3:-');

$table_prefix  = 'pc_';
define('WPLANG', 'pt_BR');

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
